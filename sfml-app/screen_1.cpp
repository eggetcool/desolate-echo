#include "stdafx.h"
#include "screen_1.hpp"
#include "cScreen.hpp"
#include "SoundManager.h"
#include "PlayerShip.h"
#include "hovercraftturret.h"
#include "Prowler.h"
#include "PlayerProjectile.h"
#include "RocketLauncher.h"
#include "LaserCannon.h"
#include "LightSystem.h"
#include "Shield.h"
#include "HeadsUp.h"
#include "MapLoad.h"
#include "SpriteManager.h"
#include "SpiderLing.h"
#include "Pause.h"
#include "Exit.h"
#include "ChrystalPickup.h"
#include "CollisionManager.h"
#include "screen_win.h"

screen_1::screen_1(void)
{
	r1.top = 20;
	r1.left = 0;
	r1.height = 57;
	r1.width = 27;
	assert(turret.loadFromFile("../data/hovercraft_turret.png"));
	//assert(unshadowShader.loadFromFile("../data/unshadowShader.vert", "../data/unshadowShader.frag"));
	//assert(lightOverShapeShader.loadFromFile("../data/lightOverShapeShader.vert", "../data/lightOverShapeShader.frag"));
	assert(penumbraTexture.loadFromFile("../data/penumbraTexture.png"));
	assert(projectile.loadFromFile("../data/bullet_proj.png"));
	assert(rocket.loadFromFile("../data/missile_proj.png"));
	assert(laser.loadFromFile("../data/laser_proj.png"));
	assert(t_shield.loadFromFile("../data/Shield.png"));
	assert(exit.loadFromFile("../data/exit.png"));
	assert(Headsupdisplay.loadFromFile("../data/HUD.png"));
	assert(crystalPT.loadFromFile("../data/crystal.png"));
	assert(boxCheckTexture.loadFromFile("../data/check_box_filled.png"));
	penumbraTexture.setSmooth(true);
	o_Hovercraftturret = nullptr;
	o_Exit = nullptr;
	o_Pause = nullptr;
	o_MapLoad = nullptr;
	o_HeadsUpDisplay = nullptr;
	o_Shield = nullptr;
	o_PlayerShip = nullptr;
	pSpriteManager = nullptr;
}
screen_1::~screen_1()
{
	
	delete o_Hovercraftturret;
	for (int x = 0; x < crystallist.size(); x++)
	{
		delete crystallist[x];
		crystallist[x] = nullptr;
		crystallist.erase(crystallist.begin() + x);
	}

	for (int x = 0; x < spiderlinglist.size(); x++)
	{
		delete spiderlinglist[x];
		spiderlinglist[x] = nullptr;
		spiderlinglist.erase(spiderlinglist.begin() + x);
	}

	for (int x = 0; x < prowlerlist.size(); x++)
	{
		delete prowlerlist[x];
		prowlerlist[x] = nullptr;
		prowlerlist.erase(prowlerlist.begin() + x);
	}
	delete o_Exit;
	delete o_Pause;
	//delete o_MapLoad;
	delete o_HeadsUpDisplay;
	delete o_Shield;
	delete o_PlayerShip;
	delete pSpriteManager;
}

int screen_1::Run(sf::RenderWindow &App, sf::Clock deltaTime)
{
	sf::Event Event;
	bool Running = true;

	playerShipSprite.setTexture(ship);
	hovercraftsprite.setTexture(turret);
	shieldSprite.setTexture(t_shield);
	exitSprite.setTexture(exit);
	playerProjSprite.setTexture(projectile);
	rocketSprite.setTexture(rocket);
	laserSprite.setTexture(laser);
	headsupSprite.setTexture(Headsupdisplay);
	crystalSprite.setTexture(crystalPT);
	shieldCheckBox.setTexture(boxCheckTexture);
	missileCheckBox.setTexture(boxCheckTexture);
	laserCheckBox.setTexture(boxCheckTexture);

	shieldCheckBox.setScale(0.61f, 0.61f);
	missileCheckBox.setScale(0.61f, 0.61f);
	laserCheckBox.setScale(0.61f, 0.61f);

	pSpriteManager = new SpriteManager();
	o_Soundmanager = new SoundManager();
	o_PlayerShip = new PlayerShip(pSpriteManager, App.getSize().x, o_Soundmanager);
	o_Shield = new Shield(shieldSprite, App.getSize().x, o_PlayerShip->GetVPos(), o_PlayerShip, o_HeadsUpDisplay, o_Soundmanager);
	o_HeadsUpDisplay = new HeadsUp(pSpriteManager, headsupSprite, viewsizeX, viewsizeY, o_PlayerShip, o_Shield);
	o_MapLoad = new MapLoad();
	o_Pause = new Pause();
	o_Exit = new Exit(exitSprite, App.getSize().x, o_PlayerShip->GetVPos(), o_PlayerShip, o_Soundmanager);
	
	NestPositions();




	//Flytta
	while (App.pollEvent(Event))
	{
		//...

		// catch the resize events
		if (Event.type == sf::Event::Resized)
		{
			// update the view to the new size of the window
			sf::FloatRect visibleArea(0, 0, Event.size.width, Event.size.height);
			App.setView(sf::View(visibleArea));
		}
	}

	for (int i = 0; i < nestPositionsProwler.size(); i++)
	{
		o_Prowler = new Prowler(pSpriteManager, nestPositionsProwler[i], o_PlayerShip->GetVPos(), o_PlayerShip, o_HeadsUpDisplay, o_Shield, o_Soundmanager);
		prowlerlist.push_back(o_Prowler);
	}
	for (int j = 0; j < nestPositionsSpider.size(); j++)
	{
		o_Spiderling = new SpiderLing(pSpriteManager, nestPositionsSpider[j], o_PlayerShip->GetVPos(), o_PlayerShip, o_HeadsUpDisplay, o_Shield, o_Soundmanager);
		spiderlinglist.push_back(o_Spiderling);
	}
	for (int x = 0; x < crystalPos.size(); x++)
	{
		o_Crystal = new CrystalPickup(crystalSprite, viewsizeX, crystalPos[x], o_PlayerShip, o_HeadsUpDisplay, o_Soundmanager);
		crystallist.push_back(o_Crystal);
	}

	o_Hovercraftturret = new hovercraftturret(hovercraftsprite, playerProjSprite, pSpriteManager, laserSprite, o_PlayerShip->GetVPos(), App.getSize().x, App.getSize().y, prowlerlist, spiderlinglist, o_RocketLauncher, o_LaserCannon, o_Soundmanager);

	//Light
	lightMapTexture.create(8640, 7040); // Make a lightmap that can cover our screen
	lightmap.setTexture(lightMapTexture.getTexture()); // Make our lightmap sprite use the correct texture

	//Hovercraft light emit
	lightTexture.loadFromFile("../data/pointLightTexture.png"); // Load in our light 
	lightTexture.setSmooth(true); // (Optional) It just smoothes the light out a bit
	light.setTexture(lightTexture); // Make our lightsprite use our loaded image
	light.setOrigin(32.0f, 32.0f);
	light.setPosition(o_Hovercraftturret->GetX(), o_Hovercraftturret->GetY());
	light.setScale(5, 5);

	//Boss light

	bossLight.setTexture(lightTexture);
	bossLight.setOrigin(32.0f, 32.0f);
	bossLight.setPosition(4450, 750);
	bossLight.setScale(15, 15);
	bossLight.setColor(sf::Color::Color(191, 62, 255));

	view1.reset(sf::FloatRect(o_PlayerShip->GetX(), o_PlayerShip->GetY(), viewsizeX, viewsizeY));
	Hudview.reset(sf::FloatRect(o_PlayerShip->GetX(), o_PlayerShip->GetY(), viewsizeX, viewsizeY));


	if (!musictrack.openFromFile("../data/ambient.wav"))
	{
		std::cout << "Peter stirrar massor" << std::endl;
	}

	//music
	musictrack.setLoop(true);
	musictrack.play();

	//////////////////////////////////
	///////////Hitbox on map//////////
	//////////////////////////////////
	//tiles = o_MapLoad->loadTiles();
	o_MapLoad->loadTiles();


	for (int x = 0; x < tileWidth; x++)
	{
		for (int y = 0; y < tileHeight; y++)
		{
			int id = o_MapLoad->level2D[y][x];
			//Make Pretty
			if (id >= 1)
			{
				sf::Vector2f pos = sf::Vector2f(x * 32, y * 32);
				sf::FloatRect rect = sf::FloatRect(pos, sf::Vector2f(32, 32));

				tileHitboxes[x][y] = rect;
			}
		}
	}


	while (Running)
	{
		sf::Time lasttime = deltaTime.restart();
		float millideltatime = lasttime.asMilliseconds() / 1000.0f;

		if (o_PlayerShip->Life(0) == 0)
		{
			return 3;
		}
		//Verifying events
		while (App.pollEvent(Event))
		{
			// Window closed
			if (Event.type == sf::Event::Closed)
			{
				return (-1);
			}
			//Key pressed
		/*	if (Event.type == sf::Event::KeyPressed)
			{
				switch (Event.key.code)
				{
				case sf::Keyboard::Escape:
					musictrack.stop();
					return (0);
					break;
				default:
					break;
				}
			}*/
			if (Event.type == sf::Event::MouseMoved)
			{
				localMousePosition = sf::Mouse::getPosition(App);
			}
		}
		view1.reset(sf::FloatRect(o_PlayerShip->GetX() - viewsizeX / 2, o_PlayerShip->GetY() - viewsizeY / 2, viewsizeX, viewsizeY));
		Hudview.reset(sf::FloatRect(o_PlayerShip->GetX() - viewsizeX / 2, o_PlayerShip->GetY() - viewsizeY / 2, viewsizeX, viewsizeY));
		light.setPosition(o_Hovercraftturret->GetX(), o_Hovercraftturret->GetY());
		Update(millideltatime, App);
		Draw(App);

		//Move to Update
		if (o_PlayerShip->getHitbox().intersects(o_Exit->getHitbox()))
		{
			musictrack.stop();
			o_PlayerShip->stopSounds();
			o_Exit->playExitSound();

			/*ending.setFillColor(sf::Color(255, 255, 255, elapsedtime));
			elapsedtime -= millideltatime * 100;
			std::cout << elapsedtime << std::endl;*/

			/*if (elapsedtime <= 0)
			{*/
				std::cout << "EXIT COMPLETE" << std::endl;
				return (4);
			/*}*/
		}
		sf::Vector2i mouse = sf::Mouse::getPosition(App);
		//particles.setEmitter(App.mapPixelToCoords(mouse));

	}
	return -1;
}

bool screen_1::Update(float deltaTime, sf::RenderWindow& App)
{
	//ending.setPosition(o_PlayerShip->getPosition().x - 400, o_PlayerShip->getPosition().y - 300);

	if (pause)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
		{
			if (!KeyPressed)
			{
				pause = false;
				musictrack.play();
			}
			KeyPressed = true;
		}

		else
		{
			KeyPressed = false;
		}
		o_Pause->pauseScreenCanvasS.setPosition(view1.getCenter().x, view1.getCenter().y);
		o_Pause->pauseScreenSprite.setPosition(view1.getCenter().x, view1.getCenter().y);

		if (o_HeadsUpDisplay->howManyCrystals() > 0)
		{
			pressed1 = sf::Keyboard::isKeyPressed(sf::Keyboard::Num1);

			if (pressed1 && !lastPressed1)
			{
				o_Shield->playShieldChargedSound();
				o_HeadsUpDisplay->GotShield();
				o_HeadsUpDisplay->RemoveCrystal(1);
				o_PlayerShip->shieldPickedUp(true);
				activepressed1 = true;
				std::cout << "Shield ACTIVATED" << std::endl;
			}
			lastPressed1 = pressed1;

			pressed2 = sf::Keyboard::isKeyPressed(sf::Keyboard::Num2);

			if (pressed2 && !lastPressed2)
			{
				o_Hovercraftturret->ActivateLaser(true);
				//o_RocketLauncher->playActivateSound();
				o_HeadsUpDisplay->RemoveCrystal(1);
				activepressed2 = true;
				std::cout << "LaserCannon ACTIVATED" << std::endl;
			}
			lastPressed2 = pressed2;

			pressed3 = sf::Keyboard::isKeyPressed(sf::Keyboard::Num3);

			if (pressed3 && !lastPressed3)
			{
				o_Hovercraftturret->ActivateRocket(true);
				o_Hovercraftturret->addRocket(3);
				std::cout << o_Hovercraftturret->howManyRockets() << std::endl;
				//o_LaserCannon->playActivateSound();
				o_HeadsUpDisplay->RemoveCrystal(1);
				activepressed3 = true;
				std::cout << "RocketLauncher ACTIVATED" << std::endl;
			}
			lastPressed3 = pressed3;
		}
		return true;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
	{
		if (!KeyPressed)
		{
			pause = true;
			musictrack.pause();
		}
		KeyPressed = true;
	}

	else
	{
		KeyPressed = false;
	}

	o_PlayerShip->Update(deltaTime);
	o_HeadsUpDisplay->Update(deltaTime, Hudview.getCenter());
	o_Hovercraftturret->Update(deltaTime, localMousePosition, o_PlayerShip->GetVPos());
	o_Hovercraftturret->RotateTurret(o_PlayerShip->GetVPos(), localMousePosition);
	o_Shield->Update(deltaTime, o_PlayerShip->GetVPos());
	o_Exit->Update(deltaTime, o_PlayerShip->GetVPos());

	//lightmap.setPosition(o_PlayerShip->GetVPos());

	playerprojV = o_Hovercraftturret->GetProjectileList();
	rocketprojV = o_Hovercraftturret->GetRocketList();
	laserprojV = o_Hovercraftturret->GetLaserList();

	pressed1 = o_Hovercraftturret->IsPressed();

	shieldCheckBox.setPosition(o_PlayerShip->GetVPos().x - 386, o_PlayerShip->GetVPos().y - 137);
	laserCheckBox.setPosition(o_PlayerShip->GetVPos().x - 386, o_PlayerShip->GetVPos().y - 36);
	missileCheckBox.setPosition(o_PlayerShip->GetVPos().x - 386, o_PlayerShip->GetVPos().y + 68);

	if (pressed1 && !lastPressed1 && o_Hovercraftturret->damnit)
	{
		camerashakecounter += deltaTime;
		if (camerashakecounter < 5)
		{
			view1.move(MathEx::S_RandomFloat(1, 3), MathEx::S_RandomFloat(1, 3));
		}
	}
	lastPressed1 = pressed1;
	

	pressed2 = o_Hovercraftturret->IsPressedR();//&& o_Hovercraftturret->IsRocket())

	if (pressed2 && !lastPressed2 && o_Hovercraftturret->rightmouseTimer < 1 && o_Hovercraftturret->howManyRockets() > 0)
	{
		o_Hovercraftturret->removeRocket(1);
		std::cout << o_Hovercraftturret->howManyRockets() << std::endl;
		camerashakecounter += deltaTime;
		if (camerashakecounter < 5)
		{
			view1.move(MathEx::S_RandomFloat(5, 10), MathEx::S_RandomFloat(5, 10));
		}
	}
	lastPressed2 = pressed2;

	pressed3 = o_Hovercraftturret->IsPressedM(); //&& o_Hovercraftturret->IsLaser())

	if (pressed3 && !lastPressed3)
	{
		camerashakecounter += deltaTime;
		if (camerashakecounter < 5)
		{
			view1.move(MathEx::S_RandomFloat(0.5, 1.5), MathEx::S_RandomFloat(0.1, 0.2));
		}
	}
	lastPressed3 = pressed3;

	auto it = prowlerlist.begin();
	while (it != prowlerlist.end())
	{
		if ((*it) != nullptr && !(*it)->isDead())
		{
			(*it)->Update(deltaTime, o_PlayerShip->GetVPos());
		}
		it++;
	}

	auto spiderit = spiderlinglist.begin();
	while (spiderit != spiderlinglist.end())
	{
		if ((*spiderit) != nullptr && !(*spiderit)->isDead())
		{
			(*spiderit)->Update(deltaTime, o_PlayerShip->GetVPos());
		}
		spiderit++;
	}
	auto crystalit = crystallist.begin();
	while (crystalit != crystallist.end())
	{
		if ((*crystalit) != nullptr && (*crystalit)->isActive())
		{
			(*crystalit)->Update(deltaTime);
		}
		crystalit++;
	}

	if (o_PlayerShip->getHitbox().intersects(o_Shield->getHitbox()) && o_Shield->IsVisible())
	{
		o_HeadsUpDisplay->GotShield();
		o_Shield->Disappear();
	}

	sf::Vector2f camPos = o_PlayerShip->GetVPos();
	camPos.x -= viewsizeX / 2;
	camPos.y -= viewsizeY / 2;
	int startXIndex = camPos.x;
	int startYIndex = camPos.y;
	int endXIndex = (camPos.x + viewsizeX);
	int endYIndex = (camPos.y + viewsizeY);

	auto rocketit = rocketprojV.begin();
	while (rocketit != rocketprojV.end())
	{
		if ((*rocketit)->GetVPos().x > endXIndex)
		{
			(*rocketit)->makeVisible(false);
		}
		if ((*rocketit)->GetVPos().x < startXIndex)
		{
			(*rocketit)->makeVisible(false);
		}
		if ((*rocketit)->GetVPos().y > endYIndex)
		{
			(*rocketit)->makeVisible(false);
		}
		if ((*rocketit)->GetVPos().y < startYIndex)
		{
			(*rocketit)->makeVisible(false);
		}
		rocketit++;
	}
	auto playerprojit = playerprojV.begin();
	while (playerprojit != playerprojV.end())
	{
		if ((*playerprojit)->GetVPos().x > endXIndex)
		{
			(*playerprojit)->Deactivate();
		}
		if ((*playerprojit)->GetVPos().x < startXIndex)
		{
			(*playerprojit)->Deactivate();
		}
		if ((*playerprojit)->GetVPos().y > endYIndex)
		{
			(*playerprojit)->Deactivate();
		}
		if ((*playerprojit)->GetVPos().y < startYIndex)
		{
			(*playerprojit)->Deactivate();
		}
		playerprojit++;
	}
	auto laserit = laserprojV.begin();
	while (laserit != laserprojV.end())
	{
		if ((*laserit)->GetVPos().x > endXIndex)
		{
			(*laserit)->Deactivate();
		}
		if ((*laserit)->GetVPos().x < startXIndex)
		{
			(*laserit)->Deactivate();
		}
		if ((*laserit)->GetVPos().y > endYIndex)
		{
			(*laserit)->Deactivate();
		}
		if ((*laserit)->GetVPos().y < startYIndex)
		{
			(*laserit)->Deactivate();
		}
		laserit++;
	}

	UpdateCollision();

	return false;
}

void screen_1::Draw(sf::RenderWindow& App)
{
	if (pause)
	{
		o_Pause->pauseScreenCanvasT.clear(sf::Color(0, 0, 0, 255));
		o_Pause->pauseScreenCanvasT.draw(o_Pause->pauseScreenSprite);
		o_Pause->pauseScreenCanvasT.display();
	}

	//Clear screen
	App.clear();
	App.setView(view1);

	////Draw objects
	sf::Vector2f camPos = o_PlayerShip->GetVPos();
	camPos.x -= viewsizeX / 2;
	camPos.y -= viewsizeY / 2;
	int startXIndex = camPos.x / 32;
	int startYIndex = camPos.y / 32;
	int endXIndex = (camPos.x + viewsizeX) / 32;
	int endYIndex = (camPos.y + viewsizeY) / 32;

	o_MapLoad->Draw(App, startXIndex, endXIndex, startYIndex, endYIndex);
	o_Shield->Draw(App);
	o_Exit->Draw(App);

	auto it = prowlerlist.begin();
	while (it != prowlerlist.end())
	{
		if ((*it) != nullptr)
		{
			(*it)->Draw(App);
		}
		it++;
	}

	auto spiderit = spiderlinglist.begin();
	while (spiderit != spiderlinglist.end())
	{
		if ((*spiderit) != nullptr)
		{
			(*spiderit)->Draw(App);
		}
		spiderit++;
	}

	auto crystalit = crystallist.begin();
	while (crystalit != crystallist.end())
	{
		if ((*crystalit) != nullptr && (*crystalit)->isActive())
		{
			(*crystalit)->Draw(App);
		}
		crystalit++;
	}

	o_PlayerShip->Draw(App);
	o_Hovercraftturret->Draw(App);

	//Clear lightmap
	lightMapTexture.clear(sf::Color::Black);

	//Draw lights
	lightMapTexture.draw(light, sf::BlendAdd); // This blendmode is used so the black in our lightimage won't be drawn to the lightmap
	//lightMapTexture.setView(view1);
	lightMapTexture.draw(bossLight, sf::BlendAdd);
	lightMapTexture.draw(o_Hovercraftturret->GetLight(), sf::BlendAdd);
	if (o_Hovercraftturret->IsPressed() && o_Hovercraftturret->damnit)
	{
		lightMapTexture.draw(o_Hovercraftturret->GetMuzzleLight(), sf::BlendAdd);
		mainshotLight = true;
	}
	if (mainshotLight)
	{
		auto it = playerprojV.begin();
		while (it != playerprojV.end())
		{
			lightMapTexture.draw((*it)->GetProjectileLight(), sf::BlendAdd);
			it++;
		}
	}

	if (o_Hovercraftturret->IsPressedR() && o_Hovercraftturret->rightmouseTimer < 1 && o_Hovercraftturret->howManyRockets() > 0)
	{
		lightMapTexture.draw(o_Hovercraftturret->GetMuzzleLightR(), sf::BlendAdd);
		if(o_Hovercraftturret->IsRocket())
			rocketLight = true;
	}
	if (rocketLight)
	{
		auto it = rocketprojV.begin();
		while (it != rocketprojV.end())
		{
			lightMapTexture.draw((*it)->GetRocketLight(), sf::BlendAdd);
			it++;
		}
	}
	if (o_Hovercraftturret->IsPressedM())
	{
		lightMapTexture.draw(o_Hovercraftturret->GetMuzzleLightM(), sf::BlendAdd);

		/*if (o_LaserCannon != nullptr)
		{
		lightMapTexture.draw(o_LaserCannon->GetLaserLight(), sf::BlendAdd);
		}*/
	}

	lightMapTexture.display(); // Need to make sure the rendertexture is displayed

							   //Draw lightmap
	lightmap.setTextureRect(sf::IntRect(0, 0, 8640, 7040)); // What from the lightmap we will draw
	lightmap.setPosition(0, 0); // Where on the backbuffer we will draw
	App.draw(lightmap, sf::BlendMultiply); // This blendmode is used to add the darkness from the lightmap with the parts where we draw ur light image show up brighter

	//Draw HUD
	App.setView(Hudview);
	o_HeadsUpDisplay->Draw(App);
	if (pause)
	{
		App.draw(o_Pause->pauseScreenCanvasS, BlendAdd);
		App.draw(o_Pause->pauseScreenSprite);
		if (activepressed1)
			App.draw(shieldCheckBox);
		
		if (activepressed2)
			App.draw(laserCheckBox);
		
		if (activepressed3)
			App.draw(missileCheckBox);

	}


	//Temp Debug of Hitboxes
	
	sf::Texture tex;
	tex.loadFromFile("../data/11.png");
	sf::Sprite sprite;
	sprite.setTexture(tex);

	/*sf::Vector2f camPos = o_PlayerShip->GetVPos();
	camPos.x -= viewsizeX / 2;
	camPos.y -= viewsizeY / 2;
	int startXIndex = camPos.x / 32;
	int startYIndex = camPos.y / 32;
	int endXIndex = (camPos.x + viewsizeX) / 32;
	int endYIndex = (camPos.y + viewsizeY) / 32;*/
	sf::Color c = sf::Color::White;
	c.a = 0;
	sprite.setColor(c);

	for (int x = startXIndex; x < endXIndex; x++)
	{
		for (int y = startYIndex; y < endYIndex; y++)
		{
			int id = o_MapLoad->level2D[y][x];
			//int id = tiles[x + y * tileWidth];

			sf::FloatRect r = tileHitboxes[x][y];

			if (endXIndex < 270 && endYIndex < 220)
			{
				sf::FloatRect r = tileHitboxes[x][y];
				sprite.setScale(r.width, r.height);
				sprite.setPosition(r.left, r.top);
				App.draw(sprite);
			}
		}
	}

	sf::FloatRect r = o_PlayerShip->GetSprite().getGlobalBounds();
	c = sf::Color::Magenta;
	c.a = 50;
	int width = 23;
	int height = 36;
	//sprite.setColor(c);
	sprite.setPosition(o_PlayerShip->GetVPos() - sf::Vector2f(width / 2, height / 2));
	sprite.setScale(width, height);
	App.draw(sprite);

	//App.draw(ending);

	App.display(); // Display the backbuffer	

}

inline void screen_1::NestPositions()
{
	sf::Vector2f nestPositionProwler1(370 * 8, 540 * 8);
	sf::Vector2f nestPositionProwler2(213 * 8, 661 * 8);
	sf::Vector2f nestPositionProwler3(362 * 8, 748 * 8);
	sf::Vector2f nestPositionProwler4(419 * 8, 354 * 8);
	sf::Vector2f nestPositionProwler5(461 * 8, 2569 * 8);
	sf::Vector2f nestPositionProwler6(563 * 8, 597 * 8);
	sf::Vector2f nestPositionProwler7(778 * 8, 645 * 8);
	sf::Vector2f nestPositionProwler8(876 * 8, 745 * 8);
	sf::Vector2f nestPositionProwler9(753 * 8, 481 * 8);
	sf::Vector2f nestPositionProwler10(973 * 8, 293 * 8);
	sf::Vector2f nestPositionProwler11(441 * 8, 199 * 8);
	sf::Vector2f nestPositionProwler12(208 * 8, 276 * 8);
	sf::Vector2f nestPositionProwler13(691 * 8, 353 * 8);
	sf::Vector2f nestPositionProwler14(705 * 8, 415 * 8);
	sf::Vector2f nestPositionProwler15(127 * 8, 494 * 8);

	nestPositionsProwler.push_back(nestPositionProwler1);
	nestPositionsProwler.push_back(nestPositionProwler2);
	nestPositionsProwler.push_back(nestPositionProwler3);
	nestPositionsProwler.push_back(nestPositionProwler4);
	nestPositionsProwler.push_back(nestPositionProwler5);
	nestPositionsProwler.push_back(nestPositionProwler6);
	nestPositionsProwler.push_back(nestPositionProwler7);
	nestPositionsProwler.push_back(nestPositionProwler8);
	nestPositionsProwler.push_back(nestPositionProwler9);
	nestPositionsProwler.push_back(nestPositionProwler10);
	nestPositionsProwler.push_back(nestPositionProwler11);
	nestPositionsProwler.push_back(nestPositionProwler12);
	nestPositionsProwler.push_back(nestPositionProwler13);
	nestPositionsProwler.push_back(nestPositionProwler14);
	nestPositionsProwler.push_back(nestPositionProwler15);

	sf::Vector2f nestPositionSpider1(522 * 8, 714 * 8);

	sf::Vector2f nestPositionSpider2(454 * 8, 629 * 8);

	sf::Vector2f nestPositionSpider3(652 * 8, 638 * 8);
	sf::Vector2f nestPositionSpider4(660 * 8, 645 * 8);

	sf::Vector2f nestPositionSpider5(771 * 8, 575 * 8);
	sf::Vector2f nestPositionSpider6(782 * 8, 585 * 8);

	sf::Vector2f nestPositionSpider7(847 * 8, 585 * 8);
	sf::Vector2f nestPositionSpider8(835 * 8, 588 * 8);
	sf::Vector2f nestPositionSpider9(839 * 8, 570 * 8);

	sf::Vector2f nestPositionSpider10(947 * 8, 447 * 8);
	sf::Vector2f nestPositionSpider11(930 * 8, 441 * 8);

	sf::Vector2f nestPositionSpider12(964 * 8, 568 * 8);
	sf::Vector2f nestPositionSpider13(965 * 8, 689 * 8);
	sf::Vector2f nestPositionSpider14(974 * 8, 681 * 8);

	sf::Vector2f nestPositionSpider15(864 * 8, 368 * 8);
	sf::Vector2f nestPositionSpider16(844 * 8, 351 * 8);

	sf::Vector2f nestPositionSpider17(927 * 8, 281 * 8);
	sf::Vector2f nestPositionSpider18(939 * 8, 295 * 8);

	sf::Vector2f nestPositionSpider19(889 * 8, 193 * 8);
	sf::Vector2f nestPositionSpider20(895 * 8, 178 * 8);
	sf::Vector2f nestPositionSpider21(911 * 8, 190 * 8);

	sf::Vector2f nestPositionSpider22(761 * 8, 219 * 8);
	sf::Vector2f nestPositionSpider23(764 * 8, 201 * 8);
	sf::Vector2f nestPositionSpider24(751 * 8, 192 * 8);

	sf::Vector2f nestPositionSpider25(507 * 8, 297 * 8);
	sf::Vector2f nestPositionSpider26(517 * 8, 305 * 8);
	sf::Vector2f nestPositionSpider27(531 * 8, 303 * 8);
	sf::Vector2f nestPositionSpider28(523 * 8, 293 * 8);

	sf::Vector2f nestPositionSpider29(169 * 8, 365 * 8);
	sf::Vector2f nestPositionSpider30(152 * 8, 361 * 8);
	sf::Vector2f nestPositionSpider31(134 * 8, 354 * 8);

	sf::Vector2f nestPositionSpider32(304 * 8, 434 * 8);
	sf::Vector2f nestPositionSpider33(290 * 8, 423 * 8);
	sf::Vector2f nestPositionSpider34(270 * 8, 425 * 8);

	sf::Vector2f nestPositionSpider35(588 * 8, 405 * 8);
	sf::Vector2f nestPositionSpider36(607 * 8, 408 * 8);
	sf::Vector2f nestPositionSpider37(607 * 8, 425 * 8);

	sf::Vector2f nestPositionSpider38(544 * 8, 424 * 8);
	sf::Vector2f nestPositionSpider39(528 * 8, 418 * 8);

	sf::Vector2f nestPositionSpider40(112 * 8, 732 * 8);
	sf::Vector2f nestPositionSpider41(127 * 8, 746 * 8);

	sf::Vector2f nestPositionSpider42(375 * 8, 649 * 8);
	sf::Vector2f nestPositionSpider43(394 * 8, 666 * 8);

	sf::Vector2f nestPositionSpider44(555 * 8, 131 * 8);
	sf::Vector2f nestPositionSpider45(567 * 8, 136 * 8);
	sf::Vector2f nestPositionSpider46(591 * 8, 135 * 8);
	sf::Vector2f nestPositionSpider47(598 * 8, 152 * 8);
	sf::Vector2f nestPositionSpider48(572 * 8, 158 * 8);
	sf::Vector2f nestPositionSpider49(551 * 8, 149 * 8);
	sf::Vector2f nestPositionSpider50(548 * 8, 173 * 8);

	nestPositionsSpider.push_back(nestPositionSpider1);
	nestPositionsSpider.push_back(nestPositionSpider2);
	nestPositionsSpider.push_back(nestPositionSpider3);
	nestPositionsSpider.push_back(nestPositionSpider4);
	nestPositionsSpider.push_back(nestPositionSpider5);
	nestPositionsSpider.push_back(nestPositionSpider6);
	nestPositionsSpider.push_back(nestPositionSpider7);
	nestPositionsSpider.push_back(nestPositionSpider8);
	nestPositionsSpider.push_back(nestPositionSpider9);
	nestPositionsSpider.push_back(nestPositionSpider10);
	nestPositionsSpider.push_back(nestPositionSpider11);
	nestPositionsSpider.push_back(nestPositionSpider12);
	nestPositionsSpider.push_back(nestPositionSpider13);
	nestPositionsSpider.push_back(nestPositionSpider14);
	nestPositionsSpider.push_back(nestPositionSpider15);
	nestPositionsSpider.push_back(nestPositionSpider16);
	nestPositionsSpider.push_back(nestPositionSpider17);
	nestPositionsSpider.push_back(nestPositionSpider18);
	nestPositionsSpider.push_back(nestPositionSpider19);
	nestPositionsSpider.push_back(nestPositionSpider20);
	nestPositionsSpider.push_back(nestPositionSpider21);
	nestPositionsSpider.push_back(nestPositionSpider22);
	nestPositionsSpider.push_back(nestPositionSpider23);
	nestPositionsSpider.push_back(nestPositionSpider24);
	nestPositionsSpider.push_back(nestPositionSpider25);
	nestPositionsSpider.push_back(nestPositionSpider26);
	nestPositionsSpider.push_back(nestPositionSpider27);
	nestPositionsSpider.push_back(nestPositionSpider28);
	nestPositionsSpider.push_back(nestPositionSpider29);
	nestPositionsSpider.push_back(nestPositionSpider30);
	nestPositionsSpider.push_back(nestPositionSpider31);
	nestPositionsSpider.push_back(nestPositionSpider32);
	nestPositionsSpider.push_back(nestPositionSpider33);
	nestPositionsSpider.push_back(nestPositionSpider34);
	nestPositionsSpider.push_back(nestPositionSpider35);
	nestPositionsSpider.push_back(nestPositionSpider36);
	nestPositionsSpider.push_back(nestPositionSpider37);
	nestPositionsSpider.push_back(nestPositionSpider38);
	nestPositionsSpider.push_back(nestPositionSpider39);
	nestPositionsSpider.push_back(nestPositionSpider40);
	nestPositionsSpider.push_back(nestPositionSpider41);
	nestPositionsSpider.push_back(nestPositionSpider42);
	nestPositionsSpider.push_back(nestPositionSpider43);
	nestPositionsSpider.push_back(nestPositionSpider44);
	nestPositionsSpider.push_back(nestPositionSpider45);
	nestPositionsSpider.push_back(nestPositionSpider46);
	nestPositionsSpider.push_back(nestPositionSpider47);
	nestPositionsSpider.push_back(nestPositionSpider48);
	nestPositionsSpider.push_back(nestPositionSpider49);
	nestPositionsSpider.push_back(nestPositionSpider50);

	sf::Vector2f crystalPosition1(623 * 8, 782 * 8);
	sf::Vector2f crystalPosition2(881 * 8, 668 * 8);
	sf::Vector2f crystalPosition3(983 * 8, 703 * 8);
	sf::Vector2f crystalPosition4(723 * 8, 489 * 8);
	sf::Vector2f crystalPosition5(995 * 8, 303 * 8);
	sf::Vector2f crystalPosition6(912 * 8, 177 * 8);
	sf::Vector2f crystalPosition7(560 * 8, 256 * 8);
	sf::Vector2f crystalPosition8(570 * 8, 256 * 8);
	sf::Vector2f crystalPosition9(559 * 8, 247 * 8);
	sf::Vector2f crystalPosition10(569 * 8, 247 * 8);
	sf::Vector2f crystalPosition11(436 * 8, 267 * 8);
	sf::Vector2f crystalPosition12(233 * 8, 276 * 8);
	sf::Vector2f crystalPosition13(416 * 8, 346 * 8);
	sf::Vector2f crystalPosition14(411 * 8, 525 * 8);
	sf::Vector2f crystalPosition15(493 * 8, 583 * 8);
	sf::Vector2f crystalPosition16(110 * 8, 748 * 8);
	sf::Vector2f crystalPosition17(362 * 8, 764 * 8);



	crystalPos.push_back(crystalPosition1);
	crystalPos.push_back(crystalPosition2);
	crystalPos.push_back(crystalPosition3);
	crystalPos.push_back(crystalPosition4);
	crystalPos.push_back(crystalPosition5);
	crystalPos.push_back(crystalPosition6);
	crystalPos.push_back(crystalPosition7);
	crystalPos.push_back(crystalPosition8);
	crystalPos.push_back(crystalPosition9);
	crystalPos.push_back(crystalPosition10);
	crystalPos.push_back(crystalPosition11);
	crystalPos.push_back(crystalPosition12);
	crystalPos.push_back(crystalPosition13);
	crystalPos.push_back(crystalPosition14);
	crystalPos.push_back(crystalPosition15);
	crystalPos.push_back(crystalPosition16);
	crystalPos.push_back(crystalPosition17);
}

void screen_1::UpdateCollision()
{
	sf::Vector2f camPos = o_PlayerShip->GetVPos();
	camPos.x -= viewsizeX / 2;
	camPos.y -= viewsizeY / 2;

	int startXIndex = camPos.x / 32;
	int startYIndex = camPos.y / 32;
	int endXIndex = (camPos.x + viewsizeX) / 32;
	int endYIndex = (camPos.y + viewsizeY) / 32;

	//One tile outside of the screen
	startXIndex -= 1;
	startYIndex -= 1;

	endXIndex += 1;
	endYIndex += 1;

	for (int x = startXIndex; x < endXIndex; x++)
	{
		for (int y = startYIndex; y < endYIndex; y++)
		{
			int id = o_MapLoad->level2D[y][x];
			//int id = tiles[x + y * tileWidth];

			sf::FloatRect r = tileHitboxes[x][y];

			//hitboxSprite.setPosition(r.left, r.top);
			//App.draw(hitboxSprite);

			sf::FloatRect intersection;
			if (tileHitboxes[x][y].intersects(o_PlayerShip->GetSprite().getGlobalBounds(), intersection))
			{
				std::cout << "hello" << std::endl;
				sf::Vector2f tilePos;
				tilePos.x = r.left + r.width / 2;
				tilePos.y = r.top + r.height / 2;
				sf::Vector2f playerPos = o_PlayerShip->GetVPos();
				sf::Vector2f diff = playerPos - tilePos;

				int xDir = (diff.x > 0) ? 1 : -1;
				int yDir = (diff.y > 0) ? 1 : -1;

				sf::Vector2f newPos = playerPos;
				if (abs(diff.x) > abs(diff.y))
					newPos.x = playerPos.x + intersection.width * xDir;
				else
					newPos.y = playerPos.y + intersection.height * yDir;

				o_PlayerShip->SetVPos(newPos);
			}

			for (int z = 0; z < playerprojV.size(); z++)
			{
				if (tileHitboxes[x][y].intersects(playerprojV[z]->GetSprite().getGlobalBounds()))
				{
					o_Hovercraftturret->playWallHitSound();
					playerprojV[z]->Deactivate();
				}
			}
			for (int z = 0; z < rocketprojV.size(); z++)
			{
				if (tileHitboxes[x][y].intersects(rocketprojV[z]->GetSprite().getGlobalBounds()))
				{
					rocketprojV[z]->makeVisible(false);
				}
			}
			for (int z = 0; z < laserprojV.size(); z++)
			{
				if (tileHitboxes[x][y].intersects(laserprojV[z]->GetSprite().getGlobalBounds()))
				{
					laserprojV[z]->Deactivate();
				}
			}
			
		}
	}
}