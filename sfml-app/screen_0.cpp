#include "stdafx.h"
#include "screen_0.hpp"
#include "cScreen.hpp"
#include "SoundManager.h"
#include <iostream>


screen_0::screen_0(void)
{
	assert(backgroundImage.loadFromFile("../data/Title.PNG"));
	assert(newGameTexture.loadFromFile("../data/new_game.png"));
	assert(ResumeGameTexture.loadFromFile("../data/resume.png"));
	assert(OptionsMenuTexture.loadFromFile("../data/options.png"));
	assert(ExitTexture.loadFromFile("../data/exit_meny.png"));
}

int screen_0::Run(sf::RenderWindow &App, sf::Clock deltaTime)
{
	view1.reset(sf::FloatRect(0, 0, 800, 600));
	sf::Event Event;
	sf::Texture Texture;
	sf::Sprite Sprite;
	
	bool Running = true;
	int menu = 0;

	sf::Sprite backgroundSprite(backgroundImage);
	backgroundSprite.setTextureRect(sf::IntRect(0, 0, 800, 600));

	newGame.setTexture(newGameTexture);
	ResumeGame.setTexture(ResumeGameTexture);
	Options.setTexture(OptionsMenuTexture);
	Exit.setTexture(ExitTexture);

	Sprite.setColor(sf::Color(255, 255, 255));

	newGame.setPosition(view1.getCenter().x - newGame.getGlobalBounds().width / 2, view1.getCenter().y - newGame.getGlobalBounds().height * 2);
	ResumeGame.setPosition(view1.getCenter().x - newGame.getGlobalBounds().width / 2, view1.getCenter().y - newGame.getGlobalBounds().height);
	Options.setPosition(view1.getCenter().x - newGame.getGlobalBounds().width / 2, view1.getCenter().y);
	Exit.setPosition(view1.getCenter().x - newGame.getGlobalBounds().width / 2, view1.getCenter().y + newGame.getGlobalBounds().height);

	if (!musictrack.openFromFile("../data/Menu_Music.wav"))
	{
		std::cout << "Peter stirrar massor" << std::endl;
	}

	//music
	musictrack.setLoop(true);
	musictrack.play();

	if (!button.openFromFile("../data/Robot_blip.wav"))
	{
		std::cout << "Peter stirrar massor" << std::endl;
	}

	while (Running)
	{
		while (App.pollEvent(Event))
		{
			if (Event.type == sf::Event::Closed)
			{
				return (-1);
			}

			if (Event.type == sf::Event::KeyPressed)
			{
				switch (Event.key.code)
				{
				case sf::Keyboard::Up:
					menu--;
					button.setPitch(1);
					button.play();
					if (menu <= 0)
						menu = 0;
					break;
				case sf::Keyboard::Down:
					menu++;
					button.setPitch(1);
					button.play();
					if (menu >= 3)
						menu = 3;
					break;
				case sf::Keyboard::Return:
					if (menu == 0)
					{
						button.setPitch(0.8);
						button.play();
						musictrack.stop();
						return(5);
					}
					else if(menu == 1)
					{
						button.setPitch(0.8);
						button.play();
						musictrack.stop();
						return (5);
					}
					else if (menu == 2)
					{
						button.setPitch(0.8);
						button.play();
						return 2;
					}
					else if (menu == 3)
					{
						button.setPitch(0.8);
						button.play();
						musictrack.stop();
						return -1;
					}
					break;
				default:
					break;
				}
			}
		}
		if (menu == 0)
		{
			newGame.setColor(sf::Color::White);
			ResumeGame.setColor(sf::Color(0 - 150 - 0));
			Options.setColor(sf::Color(0 - 150 - 0));
			Exit.setColor(sf::Color(0 - 150 - 0));
		}
		else if(menu == 1)
		{
			newGame.setColor(sf::Color(0 - 150 - 0));
			ResumeGame.setColor(sf::Color::White);
			Options.setColor(sf::Color(0 - 150 - 0));
			Exit.setColor(sf::Color(0 - 150 - 0));
		}
		else if (menu == 2)
		{
			newGame.setColor(sf::Color(0 - 150 - 0));
			ResumeGame.setColor(sf::Color(0 - 150 - 0));
			Options.setColor(sf::Color::White);
			Exit.setColor(sf::Color(0 - 150 - 0));
		}
		else if (menu == 3)
		{
			newGame.setColor(sf::Color(0 - 150 - 0));
			ResumeGame.setColor(sf::Color(0 - 150 - 0));
			Options.setColor(sf::Color(0 - 150 - 0));
			Exit.setColor(sf::Color::White);
		}

		App.clear();
		App.setView(view1);
		App.draw(Sprite);
		App.draw(backgroundSprite);
		App.draw(newGame);
		App.draw(ResumeGame);
		App.draw(Options);
		App.draw(Exit);
		App.display();
	}

	return (-1);
}
