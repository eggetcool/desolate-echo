#include "stdafx.h"
#include "Prowler.h"
#include "PlayerShip.h"
#include "HeadsUp.h"
#include "aimAtMouse.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "Shield.h"
#include "SoundManager.h"

Prowler::Prowler(SpriteManager* pSpriteManager, sf::Vector2f nestPos, sf::Vector2f SpritePos, PlayerShip* pShip, HeadsUp* plifemeter, Shield* o_Shield, SoundManager* o_Soundmanager)
{
	ProwlerFly = pSpriteManager->CreateAnimatedTexture("../data/spritesheet_prowler_flying.png");
	ProwlerAttack = pSpriteManager->CreateAnimatedTexture("../data/spritesheet_prowler_attacking.png");
	ProwlerDeath = pSpriteManager->CreateAnimatedTexture("../data/spritesheet_prowler_death.png");


	attackdeltatime = 0;

	Animation();
	if (changeAnimation)
	{
		mSpriteSheet.setTextureRect(ProwlerFly->GetRegion());
		mSpriteSheet.setTexture(ProwlerFly->GetTexture());
	}

	if (changeAnimation)
	{
		mSpriteSheet.setTextureRect(ProwlerAttack->GetRegion());
		mSpriteSheet.setTexture(ProwlerAttack->GetTexture());
	}

	if (changeAnimation)
	{
		mSpriteSheet.setTextureRect(ProwlerDeath->GetRegion());
		mSpriteSheet.setTexture(ProwlerDeath->GetTexture());
	}
	life = 2000;
	damage = 20.f;
	timeElapsed = 0;
	mVisible = true;
	mDead = false;
	mMad = false;
	bool hit = false;
	dyingforeal = false;

	this->nestPos = nestPos;
	position = nestPos;

	mSpriteSheet.setPosition(position);
	this->o_Shield = o_Shield;
	this->pShip = pShip;
	lifemeter = plifemeter;
	shipPosition = SpritePos;
	movementSpeed = (MathEx::S_RandomFloat(1.5, 2.5));
	mSpriteSheet.setOrigin(sf::Vector2f(mSpriteSheet.getGlobalBounds().width / 2, mSpriteSheet.getGlobalBounds().height / 2));

	prowlerAggroSound = o_Soundmanager->CreateSound("../data/prowler_Aggro.wav");
	prowlerAggroSound->setPitch(1.1);

	hoverHitSound = o_Soundmanager->CreateSound("../data/Hover_Hit.wav");
	hoverHitSound->setPitch(MathEx::S_RandomFloat(0.8, 1.1));

	hoverShieldHitSound = o_Soundmanager->CreateSound("../data/Hover_Shield_Hit.wav");
	hoverShieldHitSound->setPitch(MathEx::S_RandomFloat(0.9, 1.2));

	prowlerDeathSound = o_Soundmanager->CreateSound("../data/prowler_Death.wav");
	prowlerDeathSound->setPitch(MathEx::S_RandomFloat(0.9, 1.1));
}

Prowler::~Prowler()
{

}

void Prowler::Update(float deltatime, sf::Vector2f SpritePos)
{
	hitboxProwler = mSpriteSheet.getGlobalBounds();
	hitboxProwler.top = position.y - mSpriteSheet.getGlobalBounds().height / 2;
	hitboxProwler.left = position.x - mSpriteSheet.getGlobalBounds().width / 2;
	hitboxProwler.height = mSpriteSheet.getTextureRect().height;
	hitboxProwler.width = mSpriteSheet.getTextureRect().width;

	setPosition(position);
	attackdeltatime += deltatime;
	shipPosition = SpritePos;

	sf::Vector2f shipDirection = shipPosition - mSpriteSheet.getPosition();
	float length = sqrt(pow(shipDirection.x, 2) + pow(shipDirection.y, 2));


	// Fly if 60 away from player
	if (length > 60 && !dyingforeal)
	{
		ProwlerFly->Update(deltatime);
		mSpriteSheet.setTextureRect(ProwlerFly->GetRegion());
		mSpriteSheet.setTexture(ProwlerFly->GetTexture());
	}

	
	if (length < 300 && !dyingforeal)
	{
		Mad(true);
	}

	/*if (isMad() == true)
	{

		sf::Vector2f centerDirection = sf::Vector2f(nestPos) - mSpriteSheet.getPosition();
		length = sqrt(pow(centerDirection.x, 2) + pow(centerDirection.y, 2));

			ProwlerFly->Update(deltatime);
			mSpriteSheet.setTextureRect(ProwlerFly->GetRegion());
			mSpriteSheet.setTexture(ProwlerFly->GetTexture());

			if (length > 5)
			{
				normalize(centerDirection);
				position += centerDirection * movementSpeed;
				mSpriteSheet.setPosition(position);
			}
	}*/

	if (isMad() == true)
	{
		//if (length < 360 && !dyingforeal)
		//{
			if (length < 300 && length > 299)
			{
				prowlerAggroSound->play();
			}
			//Change Animation Sprite to Move/Idle
			normalize(shipDirection);
			position += shipDirection * movementSpeed;
			diff.x = SpritePos.x - position.x;
			diff.y = SpritePos.y - position.y;
			float angle = mouseAim->to_angle(mouseAim->normalized(diff));
			mSpriteSheet.setRotation(angle - 90);
			mSpriteSheet.setPosition(position);
			sf::FloatRect intersection;

			if (length <= 150)
			{
				ProwlerAttack->Update(deltatime);
				mSpriteSheet.setTextureRect(ProwlerAttack->GetRegion());
				mSpriteSheet.setTexture(ProwlerAttack->GetTexture());
			}
				if (length <= 60)
				{
					//Change Animation Sprite to Attack
					ProwlerAttack->Update(deltatime);
					mSpriteSheet.setTextureRect(ProwlerAttack->GetRegion());
					mSpriteSheet.setTexture(ProwlerAttack->GetTexture());

					if (mSpriteSheet.getGlobalBounds().intersects(pShip->GetSprite().getGlobalBounds(), intersection))
					{
						if (pShip && lifemeter != nullptr && o_Shield != nullptr && mVisible == true)
						{
							Attack();
						}
						if (mVisible == false && attackdeltatime >= 0.664f)
						{
							attackdeltatime = 0;
							mVisible = true;
						}
					}
				}
			//std::cout << attackdeltatime << std::endl;
		//}
		else
		{
			//Fix idle..
			idleafterchasetime += deltatime;
			if (idleafterchasetime > 3)
			{
				sf::Vector2f centerDirection = sf::Vector2f(nestPos) - mSpriteSheet.getPosition();
				length = sqrt(pow(centerDirection.x, 2) + pow(centerDirection.y, 2));

				if (length > 5)
				{
					normalize(centerDirection);
					position += centerDirection * movementSpeed;
					mSpriteSheet.setPosition(position);
				}
			}
		}
	}
	if (life <= 0)
	{
		timeElapsed += deltatime;
		if (timeElapsed <= 2.656f)
		{
			if (timeElapsed < 0.5)
			{
				prowlerDeathSound->play();
			}
			Mad(false);
			dyingforeal = true;
			ProwlerDeath->Update(deltatime);
			mSpriteSheet.setTextureRect(ProwlerDeath->GetRegion());
			mSpriteSheet.setTexture(ProwlerDeath->GetTexture());
		}
		else
		{
			Death();
		}
	}
}



sf::Sprite Prowler::GetSprite()
{
	return mSpriteSheet;
}

float Prowler::GetX()
{
	return position.x;
}

float Prowler::GetY()
{
	return position.y;
}

void Prowler::Life(float pLife)
{
	life += pLife;
}

bool Prowler::IsVisible()
{
	return mVisible;
}

void Prowler::Death()
{
	mDead = true;
}

void Prowler::Move(float x, float y)
{
}

Vector2f Prowler::GetVPos()
{
	return position;
}

void Prowler::Draw(sf::RenderWindow & App)
{
	App.draw(mSpriteSheet);
}

void Prowler::normalize(Vector2f& rhs)
{
	float len = sqrt(pow(rhs.x, 2) + pow(rhs.y, 2));
	if (len > 0.0f)
	{
		rhs.x /= len;
		rhs.y /= len;
	}
}

void Prowler::Attack()
{
	if (!o_Shield->isActive())
	{
		pShip->Hit(position);
		pShip->Life(-damage);
		lifemeter->GotHit(damage);
		mVisible = false;

		hoverHitSound->play();
	}
	else
	{
		pShip->Hit(position);
		lifemeter->GotHit(29.f);
		mVisible = false;

		hoverShieldHitSound->play();
	}
}

bool Prowler::isDead()
{
	return mDead;
}

sf::FloatRect Prowler::getHitbox()
{
	return hitboxProwler;
}

void Prowler::setPColorRed()
{
	mSpriteSheet.setColor(sf::Color::Red);
}

void Prowler::setPColorWhite()
{
	mSpriteSheet.setColor(sf::Color::White);
}

sf::Vector2f Prowler::GetDir()
{
	return diff;
}

AnimatedTexture* Prowler::GetTexture()
{
	return ProwlerFly;
	return ProwlerAttack;
}

void Prowler::Animation()
{
	float framefloat = 0.083;

	if (changeAnimation)
	{
		//Flying animation
		ProwlerFly->AddFrame(0, 0, 192, 128, framefloat);
		ProwlerFly->AddFrame(193, 0, 192, 128, framefloat);
		ProwlerFly->AddFrame(0, 129, 192, 128, framefloat);
		ProwlerFly->AddFrame(0, 258, 192, 128, framefloat);
		ProwlerFly->AddFrame(193, 129, 192, 128, framefloat);
		ProwlerFly->AddFrame(193, 258, 192, 128, framefloat);
		ProwlerFly->AddFrame(386, 0, 192, 128, framefloat);
		ProwlerFly->AddFrame(386, 129, 192, 128, framefloat);
		ProwlerFly->AddFrame(579, 0, 192, 128, framefloat);
		ProwlerFly->AddFrame(386, 258, 192, 128, framefloat);
		ProwlerFly->AddFrame(579, 129, 192, 128, framefloat);
		ProwlerFly->AddFrame(772, 0, 192, 128, framefloat);
		ProwlerFly->AddFrame(579, 258, 192, 128, framefloat);
		ProwlerFly->AddFrame(772, 129, 192, 128, framefloat);
		ProwlerFly->AddFrame(772, 258, 192, 128, framefloat);
		ProwlerFly->AddFrame(0, 387, 192, 128, framefloat);
	}

	if (changeAnimation)
	{
		//Attacking Animation
		ProwlerAttack->AddFrame(0, 0, 192, 128, framefloat);
		ProwlerAttack->AddFrame(193, 0, 192, 128, framefloat);
		ProwlerAttack->AddFrame(0, 129, 192, 128, framefloat);
		ProwlerAttack->AddFrame(0, 258, 192, 128, framefloat);
		ProwlerAttack->AddFrame(193, 129, 192, 128, framefloat);
		ProwlerAttack->AddFrame(193, 258, 192, 128, framefloat);
		ProwlerAttack->AddFrame(386, 0, 192, 128, framefloat);
		ProwlerAttack->AddFrame(386, 129, 192, 128, framefloat);
		ProwlerAttack->AddFrame(579, 0, 192, 128, framefloat);
		ProwlerAttack->AddFrame(386, 258, 192, 128, framefloat);
		ProwlerAttack->AddFrame(579, 129, 192, 128, framefloat);
		ProwlerAttack->AddFrame(772, 0, 192, 128, framefloat);
		ProwlerAttack->AddFrame(579, 258, 192, 128, framefloat);
		ProwlerAttack->AddFrame(772, 129, 192, 128, framefloat);
		ProwlerAttack->AddFrame(772, 258, 192, 128, framefloat);
		ProwlerAttack->AddFrame(0, 387, 192, 128, framefloat);
	}
	if (changeAnimation)
	{
		ProwlerDeath->AddFrame(0, 0, 192, 128, framefloat);
		ProwlerDeath->AddFrame(193, 0, 192, 128, framefloat);
		ProwlerDeath->AddFrame(0, 129, 192, 128, framefloat);
		ProwlerDeath->AddFrame(0, 258, 192, 128, framefloat);
		ProwlerDeath->AddFrame(193, 129, 192, 128, framefloat);
		ProwlerDeath->AddFrame(193, 258, 192, 128, framefloat);
		ProwlerDeath->AddFrame(386, 0, 192, 128, framefloat);
		ProwlerDeath->AddFrame(386, 129, 192, 128, framefloat);
		ProwlerDeath->AddFrame(579, 0, 192, 128, framefloat);
		ProwlerDeath->AddFrame(386, 258, 192, 128, framefloat);
		ProwlerDeath->AddFrame(579, 129, 192, 128, framefloat);
		ProwlerDeath->AddFrame(772, 0, 192, 128, framefloat);
		ProwlerDeath->AddFrame(579, 258, 192, 128, framefloat);
		ProwlerDeath->AddFrame(772, 129, 192, 128, framefloat);
		ProwlerDeath->AddFrame(772, 258, 192, 128, framefloat);
		ProwlerDeath->AddFrame(0, 387, 192, 128, framefloat);
		ProwlerDeath->AddFrame(0, 516, 192, 128, framefloat);
		ProwlerDeath->AddFrame(193, 387, 192, 128, framefloat);
		ProwlerDeath->AddFrame(0, 645, 192, 128, framefloat);
		ProwlerDeath->AddFrame(193, 516, 192, 128, framefloat);
		ProwlerDeath->AddFrame(386, 387, 192, 128, framefloat);
		ProwlerDeath->AddFrame(0, 774, 192, 128, framefloat);
		ProwlerDeath->AddFrame(193, 645, 192, 128, framefloat);
		ProwlerDeath->AddFrame(386, 516, 192, 128, framefloat);
		ProwlerDeath->AddFrame(579, 387, 192, 128, framefloat);
		ProwlerDeath->AddFrame(193, 774, 192, 128, framefloat);
		ProwlerDeath->AddFrame(386, 645, 192, 128, framefloat);
		ProwlerDeath->AddFrame(579, 516, 192, 128, framefloat);
		ProwlerDeath->AddFrame(772, 387, 192, 128, framefloat);
		ProwlerDeath->AddFrame(386, 774, 192, 128, framefloat);
		ProwlerDeath->AddFrame(579, 645, 192, 128, framefloat);
		ProwlerDeath->AddFrame(772, 516, 192, 128, framefloat);
	}
}

bool Prowler::IsHit()
{
	return hit;
}

void Prowler::Hit(bool gotHit)
{
	hit = gotHit;
}

void Prowler::Mad(bool pMad)
{
	mMad = pMad;
}

bool Prowler::isMad()
{
	return mMad;
}
