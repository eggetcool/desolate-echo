#include "stdafx.h"
#include "SpriteManager.h"
#include "AnimatedTexture.h"

SpriteManager::SpriteManager()
{

}

SpriteManager::~SpriteManager()
{
	//TODO: Remove all animated Textures
}


AnimatedTexture* SpriteManager::CreateAnimatedTexture(const std::string& p_sFilepath)
{
	auto it = m_apxTextures.find(p_sFilepath);
	if (it == m_apxTextures.end())
	{
		sf::Texture xTextures;
		xTextures.loadFromFile(p_sFilepath);

		m_apxTextures.insert(std::pair<std::string, sf::Texture>(p_sFilepath, xTextures));
		it = m_apxTextures.find(p_sFilepath);
	}
	AnimatedTexture* xAnimatedTexture = new AnimatedTexture (it->second);
	m_apxAnimatedTextures.push_back(xAnimatedTexture);
	
	return xAnimatedTexture;
}
