#include "stdafx.h"
#include "MathExtension.h"



float MathEx::S_ClampValue(float p_Value, float p_Min, float p_Max)
{
	return (p_Value < p_Min) ? p_Min : (p_Value > p_Max) ? p_Max : p_Value;
}


Vector2f MathEx::S_ClampVector(sf::Vector2f p_Vector, sf::Vector2f p_Min, sf::Vector2f p_Max)
{
	sf::Vector2f vector = sf::Vector2f (
		(p_Vector.x < p_Min.x) ? p_Min.x : (p_Vector.x > p_Max.x) ? p_Max.x : p_Vector.x,
		(p_Vector.y < p_Min.y) ? p_Min.y : (p_Vector.y > p_Max.y) ? p_Max.y : p_Vector.y
	);
	return vector;
}


Vector2f MathEx::S_VectorAbs(Vector2f p_Vector)
{
	return Vector2f(
		static_cast<float>(abs(p_Vector.x)),
		static_cast<float>(abs(p_Vector.y))
	);
}


float MathEx::S_VectorDistance(Vector2f p_A, Vector2f p_B)
{
	Vector2f vector = Vector2f(p_A.x - p_B.x, p_A.y - p_B.y);
	return static_cast<float>(sqrt((vector.x * vector.x) + (vector.y * vector.y)));
}


float MathEx::S_VectorMagnitude(Vector2f p_Vector)
{
	return static_cast<float>(sqrt((p_Vector.x * p_Vector.x) + (p_Vector.y * p_Vector.y)));
}


Vector2f MathEx::S_VectorNormal(Vector2f p_Vector)
{
	float mag = static_cast<float>(sqrt((p_Vector.x * p_Vector.x) + (p_Vector.y * p_Vector.y)));
	return p_Vector / mag;
}


Vector2f MathEx::S_AngledUnitVector(float p_Angle)
{
	return Vector2f (
		static_cast<float>(sin(S_Deg2Rad() * p_Angle)),
		static_cast<float>(cos(S_Deg2Rad() * p_Angle))
	);
}

float MathEx::to_angle(const Vector2f & rhs)
{
	return atan2f(rhs.y, rhs.x) * (180.0f / M_PI);
}


int MathEx::S_RandomInt(int p_RangeMin, int p_RangeMax)
{
	int maxVal = p_RangeMax - p_RangeMin;
	return rand() % maxVal + p_RangeMin;
}


float MathEx::S_RandomFloat(float p_RangeMin, float p_RangeMax)
{
	return p_RangeMin + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / p_RangeMax));
}


Vector2f MathEx::S_RandomUnitCircle()
{
	float xRand = -1.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX));
	float yRand = -1.0f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX));
	float mag = static_cast<float>(sqrt((xRand * xRand) + (yRand * yRand)));

	return (mag > 1.0f) ? Vector2f(xRand, yRand) / mag : Vector2f(xRand, yRand);
}


Vector2f MathEx::S_RandomInsideCircle(float p_RandomizeRadius)
{
	float radius = static_cast<float>(abs(p_RandomizeRadius));
	float xRand = -radius + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / radius));
	float yRand = -radius + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / radius));
	float mag = static_cast<float>(sqrt((xRand * xRand) + (yRand * yRand)));

	return (mag > radius) ? (Vector2f(xRand, yRand) / mag) * radius : Vector2f(xRand, yRand);
}


float MathEx::S_Rad2Deg()
{
	return static_cast<float>(M_PI / 180);
}


float MathEx::S_Deg2Rad()
{
	return static_cast<float>(180 / M_PI);
}



