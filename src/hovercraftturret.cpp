#include "stdafx.h"
#include "hovercraftturret.h"
#include "MathExtension.h"
#include "aimAtMouse.h"
#include "PlayerProjectile.h"
#include "RocketLauncher.h"
#include "LaserCannon.h"
#include "PlayerShip.h"
#include "SpiderLing.h"
#include "SoundManager.h"
#include "Prowler.h"
#include "SpriteManager.h"
#include "AnimatedTexture.h"
#include <math.h>

hovercraftturret::hovercraftturret(sf::Sprite pSprite, sf::Sprite projSprite, SpriteManager* pSpriteManager, sf::Sprite LaserSprite, sf::Vector2f spritePos, int viewsizeX, int viewsizeY, std::vector<Prowler*> prowlerlist, std::vector<SpiderLing*> spiderlist, RocketLauncher* o_RocketLauncher, LaserCannon* o_LaserCannon, SoundManager* o_Soundmanager)
{
	mSprite = pSprite;
	mProjSprite = projSprite;
	mLaserSprite = LaserSprite;
	mSpriteManager = pSpriteManager;
	mVisible = true;
	lastPressed = false;
	lastPressedR = false;
	lastPressedM = false;
	pressed = false;
	pressedR = false;
	pressedM = false;
	position.x = spritePos.x;
	position.y = spritePos.y;
	width = viewsizeX;
	height = viewsizeY;
	mSprite.setPosition(position);
	o_prowlerlist = prowlerlist;
	o_spiderlist = spiderlist;
	this->o_RocketLauncher = o_RocketLauncher;
	this->o_LaserCannon = o_LaserCannon;
	timeElapsed = 0;
	movementSpeed = 2.0f;
	rocketsLeft = 0;
	//this->nestPos = nestPos;
	positionP = nestPos;

	lightConeTexture.loadFromFile("../data/spookyLightTexture.png"); // Load in our light 
	lightConeTexture.setSmooth(true); // (Optional) It just smoothes the light out a bit
	muzzleflashTexture.loadFromFile("../data/pointLightTexture.png");
	muzzleflashTexture.setSmooth(true);
	muzzleflashTextureR.loadFromFile("../data/pointLightTexture.png");
	muzzleflashTextureR.setSmooth(true);
	muzzleflashTextureM.loadFromFile("../data/pointLightTexture.png");
	muzzleflashTextureM.setSmooth(true);

	lightCone.setTexture(lightConeTexture); // Make our lightsprite use our loaded image
	lightCone.setOrigin(64.0f, 128.0f);
	lightCone.setPosition(GetX(), GetY());
	lightCone.setScale(2, 4);

	muzzleflash.setTexture(muzzleflashTexture);
	muzzleflash.setScale(1, 2);
	muzzleflash.setOrigin(32.f, 52.f);
	muzzleflash.setColor(sf::Color::Yellow);

	muzzleflashR.setTexture(muzzleflashTextureR);
	muzzleflashR.setScale(1.5, 3);
	muzzleflashR.setOrigin(32.f, 48.f);
	muzzleflashR.setColor(sf::Color::Yellow);

	muzzleflashM.setTexture(muzzleflashTextureM);
	muzzleflashM.setScale(1, 2);
	muzzleflashM.setOrigin(32.f, 52.f);
	muzzleflashM.setColor(sf::Color::Red);

	projSound = o_Soundmanager->CreateSound("../data/Main_gun.wav");
	projSound->setPitch(MathEx::S_RandomFloat(0.9, 1.2));

	rocketSound = o_Soundmanager->CreateSound("../data/Rocket_Launcher.wav");
	rocketSound->setPitch(MathEx::S_RandomFloat(1, 1.2));
	rocketSound->setVolume(80);

	laserSound = o_Soundmanager->CreateSound("../data/Laser_Cannon.wav");
	laserSound->setPitch(MathEx::S_RandomFloat(0.9, 1.1));

	prowlerHitSound = o_Soundmanager->CreateSound("../data/Prowler_Hit.wav");
	prowlerHitSound->setPitch(MathEx::S_RandomFloat(0.7, 1));

	rocketHitSound = o_Soundmanager->CreateSound("../data/Rocket_Hit.wav");
	rocketHitSound->setPitch(MathEx::S_RandomFloat(0.6, 0.8));

	spiderlingHitSound = o_Soundmanager->CreateSound("../data/Spider_Hit.wav");
	spiderlingHitSound->setVolume(150);
	spiderlingHitSound->setPitch(MathEx::S_RandomFloat(0.8, 1.1));
}

hovercraftturret::~hovercraftturret()
{
	if (!playerprojV.empty())
	{
		auto it = playerprojV.begin();
		while (it != playerprojV.end())
		{
			delete (*it);
			++it;
		}
	}

	if (!rocketprojV.empty())
	{
		auto itP = rocketprojV.begin();
		while (itP != rocketprojV.end())
		{
			delete (*itP);
			++itP;
		}
	}
	
	if (!laserprojV.empty())
	{
		auto itM = laserprojV.begin();
		while (itM != laserprojV.end())
		{
			delete (*itM);
			++itM;
		}
	}
}

void hovercraftturret::Update(float deltatime, Vector2i mousePos, Vector2f spritePos)
{
	//o_playerShip = p_playerShip;
	mSprite.setOrigin(sf::Vector2f((mSprite.getLocalBounds().width / 2) - 3, (mSprite.getLocalBounds().height / 2) - 0.5));
	position.x = spritePos.x;
	position.y = spritePos.y;
	mSprite.setPosition(position);

	hitboxExplosion = explosionSprite.getGlobalBounds();
	hitboxExplosion.top = position.y - explosionSprite.getGlobalBounds().height / 2;
	hitboxExplosion.left = position.x - explosionSprite.getGlobalBounds().width / 2;
	hitboxExplosion.height = explosionSprite.getTextureRect().height;
	hitboxExplosion.width = explosionSprite.getTextureRect().width;

	lightCone.setPosition(GetX(), GetY());
	muzzleflash.setPosition(GetX(), GetY());

	pressed = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	if (pressed && !lastPressed && leftmouseTimer == 0)
	{
		projSound->play(); 

		diff.x = mousePos.x - (width / 2);
		diff.y = mousePos.y - (height / 2);
		mouseAim->normalize(diff);
		float angle = mouseAim->to_angle(diff);
		leftShot = true;
		damnit = true;

		mProjSprite.setRotation(angle + 90);
		PlayerProjectile* o_Projectile = new PlayerProjectile(mProjSprite, position, diff, width, height);
		playerprojV.push_back(o_Projectile);
		
	}
	lastPressed = pressed;

	if (leftShot)
	{
		leftmouseTimer += deltatime;
		if (leftmouseTimer > 0.1)
			damnit = false;
		if (leftmouseTimer >= 0.2)
		{
			leftmouseTimer = 0;
			leftShot = false;
		}
	}

	for (int x = 0; x < playerprojV.size(); x++)
	{
		playerprojV[x]->Update(deltatime, mousePos);
		if (!playerprojV[x]->IsActive())
		{
			//prowlerHitSound->play(); //Fix this.
			delete playerprojV[x];
			playerprojV[x] = nullptr;
			playerprojV.erase(playerprojV.begin() + x);
		}
	}

	for (int j = 0; j < o_prowlerlist.size(); j++)
	{
		for (int i = 0; i < playerprojV.size(); i++)
		{
			if (playerprojV[i]->getHitbox().intersects(o_prowlerlist[j]->getHitbox()) && !o_prowlerlist[j]->isDead())
			{
				std::cout << "Hit Prowler!" << std::endl;

				o_prowlerlist[j]->Life(-100);
				damagedprowler = j;
				o_prowlerlist[j]->setPColorRed();
				o_prowlerlist[j]->Hit(true);
				o_prowlerlist[j]->Mad(true);

				prowlerHitSound->play();

				playerprojV[i]->Deactivate();
			}
		}

		if (IsRocket())
		{

			pressedR = sf::Mouse::isButtonPressed(sf::Mouse::Right);
			if (pressedR && !lastPressedR && rightmouseTimer == 0 && rocketsLeft > 0)
			{
				rocketSound->play();

				muzzleflashR.setPosition(GetX(), GetY());
				diff.x = mousePos.x - (width / 2);
				diff.y = mousePos.y - (height / 2);
				mouseAim->normalize(diff);
				float angle = mouseAim->to_angle(diff);
				rightShot = true;
				RocketLauncher* o_Rocket = new RocketLauncher(mSpriteManager, position, diff, width, height);
				mRocketSprite = o_Rocket->GetSprite();
				mRocketSprite.setRotation(angle + 90);
				rocketprojV.push_back(o_Rocket);
				
			}
			if (rightShot)
			{
				rightmouseTimer += deltatime;
				if (rightmouseTimer >= 15)
				{
					rightmouseTimer = 0;
					rightShot = false;
				}
			}
			for (int x = 0; x < rocketprojV.size(); x++)
			{
				rocketprojV[x]->Update(deltatime, mousePos);
				if (!rocketprojV[x]->IsActive())
				{
					rocketHitSound->play();
					//S�tt en bool och spela upp animationen efter.

					delete rocketprojV[x];
					rocketprojV[x] = nullptr;
					rocketprojV.erase(rocketprojV.begin() + x);
				}
			}
			lastPressedR = pressedR;

			for (int i = 0; i < rocketprojV.size(); i++)
			{
				if (rocketprojV[i]->getHitbox().intersects(o_prowlerlist[j]->getHitbox()) && !o_prowlerlist[j]->isDead())
				{
					std::cout << "Hit Prowler!" << std::endl;

					o_prowlerlist[j]->Life(-1000);
					damagedprowler = j;

					o_prowlerlist[j]->setPColorRed();
					o_prowlerlist[j]->Hit(true);
					o_prowlerlist[j]->Mad(true);

					rocketprojV[i]->makeVisible(false);
				}
			}
		}

		if (IsLaser())
		{
			pressedM = sf::Mouse::isButtonPressed(sf::Mouse::Middle);
			if (pressedM && !lastPressedM)
			{
				laserSound->play();

				muzzleflashM.setPosition(GetX(), GetY());
				diff.x = mousePos.x - (width / 2);
				diff.y = mousePos.y - (height / 2);
				mouseAim->normalize(diff);
				float angle = mouseAim->to_angle(diff);

				mLaserSprite.setRotation(angle + 90);
				LaserCannon* o_Laser = new LaserCannon(mLaserSprite, position, diff, width, height);
				laserprojV.push_back(o_Laser);
			}

			for (int x = 0; x < laserprojV.size(); x++)
			{
				laserprojV[x]->Update(deltatime, mousePos);
				if (!laserprojV[x]->IsActive())
				{
					prowlerHitSound->play(); //Fix this.
					delete laserprojV[x];
					laserprojV[x] = nullptr;
					laserprojV.erase(laserprojV.begin() + x);
				}
			}
			lastPressedM = pressedM;

			for (int i = 0; i < laserprojV.size(); i++)
			{
				if (laserprojV[i]->getHitbox().intersects(o_prowlerlist[j]->getHitbox()) && !o_prowlerlist[j]->isDead())
				{
					std::cout << "Hit! Prowler" << std::endl;

					o_prowlerlist[j]->Life(-100);
					damagedprowler = j;

					o_prowlerlist[j]->setPColorRed();
					o_prowlerlist[j]->Hit(true);
					o_prowlerlist[j]->Mad(true);

					prowlerHitSound->play();

					//laserprojV[i]->Deactivate();
				}
			}
		}

			//std::cout << timeElapsed << std::endl;
			if (o_prowlerlist[j]->IsHit() == true)
			{
				timeElapsed += deltatime;
				if (timeElapsed >= 0.05)
				{
					o_prowlerlist[damagedprowler]->setPColorWhite();
					timeElapsed = 0;
					o_prowlerlist[j]->Hit(false);
				}
			}
		}

		for (int y = 0; y < o_spiderlist.size(); y++)
		{
			for (int z = 0; z < playerprojV.size(); z++)
			{
				if (playerprojV[z]->getHitbox().intersects(o_spiderlist[y]->getHitbox()) && !o_spiderlist[y]->isDead())
				{
					std::cout << "Hit! Spiderling" << std::endl;

					o_spiderlist[y]->Life(-100);
					damagedspider = y;

					spiderlingHitSound->play();

					o_spiderlist[y]->setPColorRed();
					o_spiderlist[y]->Hit(true);

					playerprojV[z]->Deactivate();
				}
			}

			for (int i = 0; i < rocketprojV.size(); i++)
			{
				if (rocketprojV[i]->getHitbox().intersects(o_spiderlist[y]->getHitbox()) && !o_spiderlist[y]->isDead())
				{
					std::cout << "Hit Spiderling!" << std::endl;

					o_spiderlist[y]->Life(-1000);
					damagedspider = y;

					o_spiderlist[y]->setPColorRed();
					o_spiderlist[y]->Hit(true);

					spiderlingHitSound->play();

					rocketprojV[i]->makeVisible(false);
				}
			}

			for (int i = 0; i < laserprojV.size(); i++)
			{
				if (laserprojV[i]->getHitbox().intersects(o_spiderlist[y]->getHitbox()) && !o_spiderlist[y]->isDead())
				{
					std::cout << "Hit! Spiderling" << std::endl;

					o_spiderlist[y]->Life(-100);
					damagedspider = y;

					o_spiderlist[y]->setPColorRed();
					o_spiderlist[y]->Hit(true);

					spiderlingHitSound->play();

					//laserprojV[i]->Deactivate();
				}
			}


			//std::cout << timeElapsed << std::endl;
			if (o_spiderlist[y]->IsHit() == true)
			{
				timeElapsed += deltatime;
				if (timeElapsed >= 0.05)
				{
					o_spiderlist[damagedspider]->setPColorWhite();
					timeElapsed = 0;
					o_spiderlist[y]->Hit(false);
				}
			}
		}
	}

void hovercraftturret::RotateTurret(Vector2f spritePos, Vector2i mousePos)
{
	Vector2f diff;
	diff.x = mousePos.x - (width / 2);
	diff.y = mousePos.y - (height / 2);
	float angle = mouseAim->to_angle(mouseAim->normalized(diff));
	mSprite.setRotation(angle);
	lightCone.setRotation(angle + 90);
	muzzleflash.setRotation(angle + 90);
	muzzleflashR.setRotation(angle + 90);
	muzzleflashM.setRotation(angle + 90);
}

sf::Sprite hovercraftturret::GetSprite()
{
	return mSprite;
}

void hovercraftturret::Draw(sf::RenderWindow& App)
{
	for (int x = 0; x < playerprojV.size(); x++)
	{
		App.draw(playerprojV[x]->GetSprite());
	}

	for (int x = 0; x < rocketprojV.size(); x++)
	{
		App.draw(rocketprojV[x]->GetSprite());
	}

	for (int x = 0; x < laserprojV.size(); x++)
	{
		App.draw(laserprojV[x]->GetSprite());
	}
	App.draw(mSprite);
}

float hovercraftturret::GetX()
{
	return position.x;
}

float hovercraftturret::GetY()
{
	return position.y;
}

bool hovercraftturret::IsVisible()
{
	return mVisible;
}

bool hovercraftturret::IsPressed()
{
	return pressed;
}

bool hovercraftturret::IsPressedR()
{
	return pressedR;
}

bool hovercraftturret::IsPressedM()
{
	return pressedM;
}

sf::Sprite hovercraftturret::GetLight()
{
	return lightCone;
}

sf::Sprite hovercraftturret::GetMuzzleLight()
{
	return muzzleflash;
}

sf::Sprite hovercraftturret::GetMuzzleLightR()
{
	return muzzleflashR;
}

sf::Sprite hovercraftturret::GetMuzzleLightM()
{
	return muzzleflashM;
}

sf::Sprite hovercraftturret::GetTrailLight()
{
	return o_RocketLauncher->GetRocketLight();
}

sf::Vector2f hovercraftturret::GetDir()
{
	return diff;
}

void hovercraftturret::Move(float x, float y)
{
	position.x = position.x + x;
	position.y = position.y + y;
}

void hovercraftturret::normalize(sf::Vector2f & rhs)
{
	float len = sqrt(pow(rhs.x, 2) + pow(rhs.y, 2));
	if (len > 0.0f)
	{
		rhs.x /= len;
		rhs.y /= len;
	}
}

void hovercraftturret::ActivateRocket(bool activate)
{
	rocketactivated = activate;
}

void hovercraftturret::ActivateLaser(bool activate)
{
	laseractivated = activate;
}

bool hovercraftturret::IsRocket()
{
	return rocketactivated;
}

bool hovercraftturret::IsLaser()
{
	return laseractivated;
}

int hovercraftturret::howManyRockets()
{
	return rocketsLeft;
}

void hovercraftturret::addRocket(int value)
{
	rocketsLeft += value;
}

void hovercraftturret::removeRocket(int value)
{
	rocketsLeft -= value;
}

std::vector<PlayerProjectile*> hovercraftturret::GetProjectileList()
{
	return playerprojV;
}

std::vector<RocketLauncher*> hovercraftturret::GetRocketList()
{
	return rocketprojV;
}

std::vector<LaserCannon*> hovercraftturret::GetLaserList()
{
	return laserprojV;
}

void hovercraftturret::playWallHitSound()
{
	prowlerHitSound->setPitch(MathEx::S_RandomFloat(1.2, 1.4));
	prowlerHitSound->setVolume(50);
	prowlerHitSound->play();
}
