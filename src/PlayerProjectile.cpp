#include "stdafx.h"
#include "PlayerProjectile.h"
#include "Sprite.h"
#include "aimAtMouse.h"

PlayerProjectile::PlayerProjectile(sf::Sprite pSprite, Vector2f position, Vector2f direction, int windowsizeX, int windowsizeY)
{
	mVisible = true;
	mActive = true;
	maxspeed = 800.0f;
	currentSpeed = 1.0f;
	accel = 1.0f;
	width = windowsizeX;
	height = windowsizeY;
	this->direction = direction;
	this->position = position;
	mSprite = pSprite;
	mSprite.setPosition(position);
	mSprite.setOrigin(((mSprite.getLocalBounds().width / 2) - 0.5), (mSprite.getLocalBounds().height / 2));
	mSprite.setScale(1.3, 1);

	projectileLightTexture.loadFromFile("../data/pointLightTexture.png");
	projectileLightTexture.setSmooth(true);

	projectileLight.setTexture(projectileLightTexture);
	projectileLight.setPosition(GetVPos().x, GetVPos().y);
	projectileLight.setOrigin(mSprite.getOrigin().x + 25, mSprite.getOrigin().y + 20);
	projectileLight.setScale(0.5f, 1.5f);
	projectileLight.setColor(sf::Color::Yellow);
}

PlayerProjectile::~PlayerProjectile()
{
}

void PlayerProjectile::Update(float deltatime, Vector2i mousePos)
{
	hitboxProjectile = mSprite.getGlobalBounds();
	
	hitboxProjectile.top = position.y - mSprite.getGlobalBounds().height / 2;
	hitboxProjectile.left = position.x - mSprite.getGlobalBounds().width / 2;
	projectileLight.setPosition(GetVPos().x, GetVPos().y);

	float angle = MathEx::to_angle(direction);

	projectileLight.setRotation(angle + 90);

	position += direction * maxspeed * deltatime;
	mSprite.setPosition(position);
	if (position.x > 8640)
	{
		mActive = false;
	}
	if (position.y > 7040)
	{
		mActive = false;
	}
	if (position.x < 0)
	{
		mActive = false;
	}
	if (position.y < 0)
	{
		mActive = false;
	}
}

sf::Sprite PlayerProjectile::GetSprite()
{
	return mSprite;
}

bool PlayerProjectile::IsVisible()
{
	return mVisible;
}

bool PlayerProjectile::IsActive()
{
	return mActive;
}

void PlayerProjectile::Deactivate()
{
	mActive = false;
}

void PlayerProjectile::Activate()
{
	mActive = true;
}

void PlayerProjectile::Move(float x, float y)
{
	position.x = position.x + x;
	position.y = position.y + y;
}

Vector2f PlayerProjectile::GetVPos()
{
	return position;
}

sf::Sprite PlayerProjectile::GetProjectileLight()
{
	return projectileLight;
}

sf::FloatRect PlayerProjectile::getHitbox()
{
	return hitboxProjectile;
}
