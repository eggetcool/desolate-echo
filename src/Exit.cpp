#include "stdafx.h"
#include "Exit.h"

Exit::Exit(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f SpritePos, PlayerShip * o_PlayerShip, SoundManager* o_Soundmanager)
{
	mSprite = pSprite;
	mScreenWidth = pScreenWidth;
	mVisible = true;

	position.x = 570 * 8;
	position.y = 107 * 8;

	shipPosition = SpritePos;

	mSprite.setOrigin(sf::Vector2f(mSprite.getLocalBounds().width / 2, mSprite.getLocalBounds().height / 2));
	mSprite.setScale(0.2f, 0.2f);
	mSprite.setColor(sf::Color(255, 255, 255, 0));

	exitSound = o_Soundmanager->CreateSound("../data/exit.wav");
}

Exit::~Exit()
{
}

void Exit::Update(float deltatime, sf::Vector2f SpritePos)
{
	hitboxExit = mSprite.getGlobalBounds();
	hitboxExit.top = position.y;
	hitboxExit.left = position.x;

	shipPosition = SpritePos;

	mSprite.setPosition(position);
}

sf::Sprite Exit::GetSprite()
{
	return mSprite;
}

float Exit::GetX()
{
	return position.x;
}

float Exit::GetY()
{
	return position.y;
}

bool Exit::IsVisible()
{
	return mVisible;
}

void Exit::Disappear()
{
	mVisible = false;
}

void Exit::playExitSound()
{
	exitSound->setVolume(150);
	exitSound->play();
}

Vector2f Exit::GetVPos()
{
	return position;
}

void Exit::Draw(sf::RenderWindow & App)
{
	App.draw(mSprite);
}

sf::FloatRect Exit::getHitbox()
{
	return hitboxExit;
}
