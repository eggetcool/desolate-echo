#include "stdafx.h"
#include "screens.hpp"

int main(int argc, char** argv)
{
	bool vSyncEnabled = true;
	std::vector<cScreen*> Screens;
	int screen = 0;

	sf::RenderWindow App(sf::VideoMode(1024, 768, 32), "Desolate Echo");

	App.setMouseCursorVisible(true);
	App.setFramerateLimit(60);
	App.setVerticalSyncEnabled(vSyncEnabled);
	sf::Clock deltaTime;
	App.setMouseCursorVisible(false);
	
	screen_0 s0;
	Screens.push_back(&s0);
	screen_1 s1;
	Screens.push_back(&s1);
	screen_options s2;
	Screens.push_back(&s2);
	screen_end s3;
	Screens.push_back(&s3);
	screen_win s4;
	Screens.push_back(&s4);
	screen_tut s5;
	Screens.push_back(&s5);
	
	while (App.isOpen())
	{
		//Easy to handle different screens without deleting them.
		while (screen >= 0)
		{
			screen = Screens[screen]->Run(App, deltaTime);
			if (screen < 0)
				App.close();
		}
		sf::Event event;
		while (App.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				App.close();
		}

		App.clear();
		App.display();
	}
	
	return EXIT_SUCCESS;
}