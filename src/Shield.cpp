#include "stdafx.h"
#include "Shield.h"
#include "HeadsUp.h"
#include "PlayerShip.h"
#include "SoundManager.h"
#include "SpriteManager.h"
#include "AnimatedTexture.h"

Shield::Shield(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f SpritePos, PlayerShip* o_PlayerShip, HeadsUp* o_HeadsUpDisplay, SoundManager* o_Soundmanager)
{
	
	
	this->o_PlayerShip = o_PlayerShip;
	mSprite = pSprite;
	mScreenWidth = pScreenWidth;
	mVisible = true;
	position.x = 2100;
	position.y = 1900;
	shipPosition = SpritePos;
	active = false;
	pickUp = false;
	deactive = false;

	mSprite.setOrigin(mSprite.getLocalBounds().width / 2, mSprite.getLocalBounds().height / 2);

	shieldChargedSound = o_Soundmanager->CreateSound("../data/Shield_Charged.wav");

	shieldDepletedSound = o_Soundmanager->CreateSound("../data/Shield_Depleted.wav");

	shieldChargingSound = o_Soundmanager->CreateSound("../data/Shield_Recharging.wav");
	shieldChargingSound->setVolume(150);
}

Shield::~Shield()
{

}

void Shield::Update(float deltatime, sf::Vector2f SpritePos)
{
	hitboxShield = mSprite.getGlobalBounds();
	hitboxShield.top = position.y;
	hitboxShield.left = position.x;

	shipPosition = SpritePos;

	mSprite.setPosition(position);
}

sf::Sprite Shield::GetSprite()
{
	return mSprite;
}

float Shield::GetX()
{
	return position.x;
}

float Shield::GetY()
{
	return position.y;
}

bool Shield::IsVisible()
{
	return mVisible;
}

void Shield::Disappear()
{
	mVisible = false;
	o_PlayerShip->shieldPickedUp(true);
}

void Shield::PickUp(bool pPickUp)
{
	pickUp = pPickUp;
}

bool Shield::isPickedUp()
{
	return pickUp;
}

void Shield::Activate(bool pActive)
{
	active = pActive;
}

bool Shield::isActive()
{
	return active;
}

void Shield::Deactivate(bool pDeactive)
{
	deactive = pDeactive;
}

bool Shield::isDeactive()
{
	return deactive;
}

void Shield::playShieldDepletedSound()
{
	shieldDepletedSound->play();
}

void Shield::playShieldChargedSound()
{
	shieldChargedSound->play();
}

void Shield::playShieldChargingSound()
{
	shieldChargingSound->play();
}

Vector2f Shield::GetVPos()
{
	return position;
}

void Shield::Draw(sf::RenderWindow & App)
{
	if (mVisible)
	{
		App.draw(mSprite);
	}
}

sf::FloatRect Shield::getHitbox()
{
	return hitboxShield;
}
