#include "stdafx.h"
#include "SoundManager.h"

SoundManager::SoundManager()
{

}


SoundManager::~SoundManager()
{



}

bool SoundManager::Initialize()
{
	return true;
}

void SoundManager::Shutdown()
{
	auto it = m_apxSounds.begin();
	while (it != m_apxSounds.end())
	{
		delete (*it);
		it++;
	}
	m_apxSounds.clear();

	auto it2 = m_apxMusic.begin();
	while (it2 != m_apxMusic.end())
	{
		delete (*it2);
		it2++;
	}
	m_apxMusic.clear();

}

sf::Sound* SoundManager::CreateSound(const std::string& p_sFilepath)
{
	auto it = m_apxAudio.find(p_sFilepath);
	if (it == m_apxAudio.end())
	{
		sf::SoundBuffer xAudio;
		xAudio.loadFromFile(p_sFilepath.c_str());
		m_apxAudio.insert(std::pair<std::string, sf::SoundBuffer>(p_sFilepath, xAudio));
		it = m_apxAudio.find(p_sFilepath);
	}
	sf::Sound* xSound = new sf::Sound();
	xSound->setBuffer((it->second));
	m_apxSounds.push_back(xSound);
	return xSound;
}

void SoundManager::DestroySound(sf::Sound* p_pxSound)
{
	auto it = m_apxSounds.begin();
	while (it != m_apxSounds.end())
	{
		if ((*it) == p_pxSound)
		{
			delete (*it);
			m_apxSounds.erase(it);
			return;
		}
		it++;
	}
}

sf::Music* SoundManager::CreateMusic(const std::string & p_sFilepath)
{
	sf::Music* xMusic = new sf::Music();
	xMusic->openFromFile(p_sFilepath.c_str());
	m_apxMusic.push_back(xMusic);
	return xMusic;
}

void SoundManager::DestroyMusic(sf::Music * p_pxMusic)
{
	auto it = m_apxMusic.begin();
	while (it != m_apxMusic.end())
	{
		if ((*it) == p_pxMusic)
		{
			delete (*it);
			m_apxMusic.erase(it);
			return;
		}
		it++;
	}
}


