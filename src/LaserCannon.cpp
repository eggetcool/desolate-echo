#include "stdafx.h"
#include "Sprite.h"
#include "aimAtMouse.h"
#include "LaserCannon.h"

LaserCannon::LaserCannon(sf::Sprite pSprite, Vector2f position, Vector2f direction, int windowsizeX, int windowsizeY)
{
	mVisible = true;
	mActive = true;
	maxspeed = 200.0f;
	currentSpeed = 1.5f;
	accel = 1.0f;
	width = windowsizeX;
	height = windowsizeY;
	this->direction = direction;
	this->position = position;
	mSprite = pSprite;
	mSprite.setPosition(position);
	mSprite.setOrigin(((mSprite.getLocalBounds().width / 2) - 0.5), (mSprite.getLocalBounds().height / 2) + 5);
	mSprite.setScale(0.75, 2);
	mSprite.setColor(sf::Color::Red);

	LaserLightTexture.loadFromFile("../data/pointLightTexture.png");
	LaserLightTexture.setSmooth(true);

	LaserLight.setTexture(LaserLightTexture);
	LaserLight.setScale(1, 1);
	LaserLight.setOrigin(32.f, 64.f);
	LaserLight.setPosition(GetVPos().x, GetVPos().y);
	LaserLight.setColor(sf::Color::Red);
}

LaserCannon::~LaserCannon()
{
}

void LaserCannon::Update(float deltatime, Vector2i mousePos)
{
	hitboxLaser = mSprite.getGlobalBounds();
	hitboxLaser.top = position.y;
	hitboxLaser.left = position.x;
	LaserLight.setPosition(GetVPos().x, GetVPos().y);

	//sf::RectangleShape hitboxOutline(sf::Vector2f(hitboxProjectile.left, hitboxProjectile.top));
	//hitboxOutline.setFillColor(sf::Color(100, 250, 50));
	//hitboxOutline.setPosition(position.x, position.y);

	position += direction * maxspeed * deltatime;
	mSprite.setPosition(position);
}

sf::Sprite LaserCannon::GetSprite()
{
	return mSprite;
}

bool LaserCannon::IsVisible()
{
	return mVisible;
}

bool LaserCannon::IsActive()
{
	return mActive;
}

void LaserCannon::Deactivate()
{
	mActive = false;
}

void LaserCannon::Activate()
{
	mActive = true;
}

void LaserCannon::playActivateSound()
{
	//Fix to the right sound
}

void LaserCannon::Move(float x, float y)
{
	position.x = position.x + x;
	position.y = position.y + y;
}

Vector2f LaserCannon::GetVPos()
{
	return position;
}

sf::Sprite LaserCannon::GetLaserLight()
{
	return LaserLight;
}

sf::FloatRect LaserCannon::getHitbox()
{
	return hitboxLaser;
}
