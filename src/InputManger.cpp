#include "stdafx.h"
#include "InputManager.h"


InputManager::InputManager(sf::Window* p_pWindow)
{
	m_pWindow = p_pWindow;

	for (int i = 0; i < 3; i++)
		m_MouseButtonState[i] = false;

	for (int i = 0; i < 101; i++)
	{
		m_KeyState[i] = false;
		m_KeyDownState[0][i] = true;
		m_KeyDownState[1][i] = false;
		m_KeyUpState[0][i] = true;
		m_KeyUpState[1][i] = false;
	}
		
	m_AxisInput.m_Horizontal_Neg = sf::Keyboard::A;
	m_AxisInput.m_Horizontal_Pos = sf::Keyboard::D;
	m_AxisInput.m_Vertical_Neg = sf::Keyboard::S;
	m_AxisInput.m_Vertical_Pos = sf::Keyboard::D;
}


InputManager::~InputManager()
{

}


bool InputManager::Update(sf::Time p_DeltaTime)
{
	float deltaTime = p_DeltaTime.asSeconds();
	bool returnValue = true;

	for (int i = 0; i < 101; i++)
	{
		m_KeyDownState[1][i] = false;
		m_KeyUpState[1][i] = false;
	}
	for (int i = 0; i < 3; i++)
	{
		m_MouseButtonDown[1][i] = false;
		m_MouseButtonUp[1][i] = false;
	}

	sf::Event _event;

	while (m_pWindow->pollEvent(_event))
	{
		switch (_event.type)
		{
		case sf::Event::Closed: returnValue = false;
			break;
		case sf::Event::KeyPressed:
			OnKeyChange(_event.key.code, true);
			break;
		case sf::Event::KeyReleased:
			OnKeyChange(_event.key.code, false);
			break;
		case sf::Event::MouseButtonPressed:
			OnMouseButtonChange(_event.mouseButton.button, true);
			break;
		case sf::Event::MouseButtonReleased:
			OnMouseButtonChange(_event.mouseButton.button, false);
			break;
		case sf::Event::MouseMoved:
			m_MousePos = sf::Vector2i(_event.mouseMove.x, _event.mouseMove.x);
			break;
		case sf::Event::TextEntered:
			break;
			/*
		case sf::Event::LostFocus:
			OnFocusChange(false);
			break;
		case sf::Event::GainedFocus:
			OnFocusChange(true);
			break;
			*/
		}
	}

	VerticalInput(deltaTime);
	HorizontalInput(deltaTime);

	return returnValue;
}


void InputManager::OnKeyChange(sf::Keyboard::Key p_KeyCode, bool p_Value)
{
	int index = static_cast<int>(p_KeyCode);

	if (p_Value)
	{
		m_KeyUpState[0][index] = true;

		if (m_KeyDownState[0][index]) {
			m_KeyDownState[0][index] = false;
			m_KeyDownState[1][index] = true;
		}
	}
	else
	{
		m_KeyDownState[0][index] = true;

		if (m_KeyUpState[0][index]) {
			m_KeyUpState[0][index] = false;
			m_KeyUpState[1][index] = true;
		}
	}

	m_KeyState[index] = p_Value;
}


void InputManager::OnMouseButtonChange(int p_Index, bool p_Value)
{
	if (p_Value)
	{
		m_MouseButtonUp[0][p_Index] = true;

		if (m_MouseButtonDown[0][p_Index]) {
			m_MouseButtonDown[0][p_Index] = false;
			m_MouseButtonDown[1][p_Index] = true;
		}
	}
	else
	{
		m_MouseButtonDown[0][p_Index] = true;

		if (m_MouseButtonUp[0][p_Index]) {
			m_MouseButtonUp[0][p_Index] = false;
			m_MouseButtonUp[1][p_Index] = true;
		}
	}

	m_MouseButtonState[p_Index] = p_Value;
}


bool InputManager::GetKey(sf::Keyboard::Key p_KeyCode)
{
	int index = static_cast<int>(p_KeyCode);
	return m_KeyState[index];
}


bool InputManager::GetKeyDown(sf::Keyboard::Key p_KeyCode)
{
	int index = static_cast<int>(p_KeyCode);

	return m_KeyDownState[1][index];
}


bool InputManager::GetKeyUp(sf::Keyboard::Key p_KeyCode)
{
	int index = static_cast<int>(p_KeyCode);

	return m_KeyUpState[1][index];
}


bool InputManager::GetMouseButton(int p_Index)
{
	return m_MouseButtonState[p_Index];
}


bool InputManager::GetMouseButtonDown(int p_Index)
{
	return m_MouseButtonDown[1][p_Index];
}


bool InputManager::GetMouseButtonUp(int p_Index)
{
	return m_MouseButtonUp[1][p_Index];
}


sf::Vector2i InputManager::GetMousePosition()
{
	return m_MousePos;
}


void InputManager::HorizontalInput(float p_DeltaTime)
{
	if (GetKey(m_AxisInput.m_Horizontal_Neg) && m_AxisInput.m_Horizontal > -1.0f)
	{
		m_AxisInput.m_Horizontal -= (m_AxisInput.m_AxisGravity * p_DeltaTime);

		if (m_AxisInput.m_Horizontal < -1.0f) {
			m_AxisInput.m_Horizontal = -1.0f;
		}
	}
	else if (GetKey(m_AxisInput.m_Horizontal_Pos) && m_AxisInput.m_Horizontal < 1.0f)
	{
		m_AxisInput.m_Horizontal += (m_AxisInput.m_AxisGravity * p_DeltaTime);

		if (m_AxisInput.m_Horizontal > 1.0f) {
			m_AxisInput.m_Horizontal = 1.0f;
		}
	}
	else
	{
		if (m_AxisInput.m_Horizontal < 0.0f)
		{
			m_AxisInput.m_Horizontal += (m_AxisInput.m_AxisGravity * p_DeltaTime);
			if (m_AxisInput.m_Horizontal > 0.0f) {
				m_AxisInput.m_Horizontal = 0.0f;
			}
		}
		else if (m_AxisInput.m_Horizontal > 0.0f)
		{
			m_AxisInput.m_Horizontal -= (m_AxisInput.m_AxisGravity * p_DeltaTime);
			if (m_AxisInput.m_Horizontal < 0.0f) {
				m_AxisInput.m_Horizontal = 0.0f;
			}
		}
	}
}


void InputManager::VerticalInput(float p_DeltaTime)
{
	if (GetKey(m_AxisInput.m_Vertical_Neg) && m_AxisInput.m_Vertical > -1.0f)
	{
		m_AxisInput.m_Vertical -= (m_AxisInput.m_AxisGravity * p_DeltaTime);

		if (m_AxisInput.m_Vertical < -1.0f) {
			m_AxisInput.m_Vertical = -1.0f;
		}
	}
	else if (GetKey(m_AxisInput.m_Vertical_Pos) && m_AxisInput.m_Vertical < 1.0f)
	{
		m_AxisInput.m_Vertical += (m_AxisInput.m_AxisGravity * p_DeltaTime);

		if (m_AxisInput.m_Vertical > 1.0f) {
			m_AxisInput.m_Vertical = 1.0f;
		}
	}
	else
	{
		if (m_AxisInput.m_Vertical < 0.0f)
		{
			m_AxisInput.m_Vertical += (m_AxisInput.m_AxisGravity * p_DeltaTime);
			if (m_AxisInput.m_Vertical > 0.0f) {
				m_AxisInput.m_Vertical = 0.0f;
			}
		}
		else if (m_AxisInput.m_Vertical > 0.0f)
		{
			m_AxisInput.m_Vertical -= (m_AxisInput.m_AxisGravity * p_DeltaTime);
			if (m_AxisInput.m_Vertical < 0.0f) {
				m_AxisInput.m_Vertical = 0.0f;
			}
		}
	}
}


float InputManager::GetAxis(InputAxis p_InputAxis)
{
	switch (p_InputAxis)
	{
	case InputAxis::InputAxis_Horizontal:
		return m_AxisInput.m_Horizontal;
		break;
	case InputAxis::InputAxis_Vertical:
		return m_AxisInput.m_Vertical;
		break;
	case InputAxis::InputAxis_MouseX:
		return m_AxisInput.m_MouseX;
		break;
	case InputAxis::InputAxis_MouseY:
		return m_AxisInput.m_MouseY;
		break;
	default:
		return 0.0f;
		break;
	}
}


int InputManager::GetAxisRaw(InputAxis p_InputAxis)
{
	switch (p_InputAxis)
	{
	case InputAxis::InputAxis_Horizontal:
		return static_cast<int>(nearbyintf(m_AxisInput.m_Horizontal));
		break;
	case InputAxis::InputAxis_Vertical:
		return static_cast<int>(nearbyintf(m_AxisInput.m_Vertical));
		break;
	case InputAxis::InputAxis_MouseX:
		return static_cast<int>(nearbyintf(m_AxisInput.m_MouseX));
		break;
	case InputAxis::InputAxis_MouseY:
		return static_cast<int>(nearbyintf(m_AxisInput.m_MouseY));
		break;
	default:
		return 0;
		break;
	}
}

/*
void InputManager::OnFocusChange(bool p_Value)
{

}
*/


