#pragma once
#include "stdafx.h"
#include "SoundManager.h"

class PlayerShip;
class SoundManager;

class Exit : public sf::Transformable
{
public:
	Exit(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f SpritePos, PlayerShip* o_PlayerShip, SoundManager* o_Soundmanager);
	~Exit();
	void Update(float deltatime, sf::Vector2f SpritePos);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	bool IsVisible();
	void Disappear();
	void playExitSound();
	Vector2f GetVPos();
	void Draw(sf::RenderWindow& App);
	sf::FloatRect getHitbox();

private:
	Exit() {};

	bool mVisible;
	float mX;
	float mY;
	int mScreenWidth;

	sf::Sprite mSprite;
	sf::Vector2f shipPosition;
	sf::Vector2f position;
	sf::FloatRect hitboxExit;
	PlayerShip* o_PlayerShip;
	SoundManager* o_Soundmanager;

	sf::Sound* exitSound;
};

