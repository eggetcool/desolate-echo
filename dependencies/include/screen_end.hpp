#include "stdafx.h"
#include "cScreen.hpp"


class screen_end : public cScreen
{
private:
	sf::View view1;
	sf::VideoMode vidMode;
	sf::Texture backgroundImage;
public:
	screen_end(void);
	virtual int Run(sf::RenderWindow &App, sf::Clock deltaTime);

	float timer;
};