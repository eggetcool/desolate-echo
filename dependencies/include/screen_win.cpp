#include "stdafx.h"
#include "screen_win.h"
#include "cScreen.hpp"
#include "Exit.h"
#include "PlayerShip.h"

screen_win::screen_win(void)
{
	view1.reset(sf::FloatRect(0, 0, 800, 600));
	assert(backgroundImage.loadFromFile("../data/end_screen.PNG"));
	backgroundImage.setRepeated(true);
}


int screen_win::Run(sf::RenderWindow &App, sf::Clock deltaTime)
{
	sf::Event Event;
	bool Running = true;
	sf::Sprite backgroundSprite(backgroundImage);
	backgroundSprite.setTextureRect(sf::IntRect(0, 0, App.getSize().x, App.getSize().y));

	while (Running)
	{
		sf::Time lasttime = deltaTime.restart();
		float millideltatime = lasttime.asMilliseconds() / 1000.0f;

		//Verifying events
		while (App.pollEvent(Event))
		{
			// Window closed
			if (Event.type == sf::Event::Closed)
			{
				return (-1);
			}
			//Key pressed
			if (Event.type == sf::Event::KeyPressed)
			{
				switch (Event.key.code)
				{
				case sf::Keyboard::Escape:
					return (0);
					break;
				case sf::Keyboard::Return:			//kanske fel.
					return (1);
					break;
				default:
					break;
				}
			}
		}
		//Clearing screen
		App.clear(sf::Color(0, 0, 0, 255));
		App.setView(view1);
		App.draw(backgroundSprite);
		App.display();
	}

	//Never reaching this point normally, but just in case, exit the application
	return -1;
}