#pragma once

class sf::Sound;
class sf::Music;

class SoundManager
{
public:
	SoundManager();
	~SoundManager();

	bool Initialize();
	void Shutdown();


	sf::Sound* CreateSound(const std::string& p_sFilepath);
	void DestroySound(sf::Sound* p_pxSound);

	sf::Music* CreateMusic(const std::string& p_sFilepath);
	void DestroyMusic(sf::Music* p_pxMusic);

private:
	std::vector<sf::Sound*> m_apxSounds;
	std::map<std::string, sf::SoundBuffer> m_apxAudio;
	std::vector<sf::Music*> m_apxMusic;
	std::vector<std::string> m_apxSongs;
};