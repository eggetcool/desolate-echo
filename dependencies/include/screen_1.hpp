#include "stdafx.h"
#include "cScreen.hpp"
#include "SoundManager.h"
#include "PlayerShip.h"
#include "hovercraftturret.h"
#include "Prowler.h"
#include "PlayerProjectile.h"
#include "RocketLauncher.h"
#include "LaserCannon.h"
#include "LightSystem.h"
#include "Shield.h"
#include "HeadsUp.h"
#include "MapLoad.h"
#include "SpriteManager.h"
#include "SpiderLing.h"
#include "Pause.h"
#include "Exit.h"
#include "ChrystalPickup.h"
#include "CollisionManager.h"

class screen_1 : public cScreen
{
public:
	screen_1(void);
	~screen_1();
	virtual int Run(sf::RenderWindow &App, sf::Clock deltaTime);
	bool Update(float deltatime, sf::RenderWindow& App);
	void Draw(sf::RenderWindow & App);
	void NestPositions();
	void UpdateCollision();
private:
	sf::View view1;
	sf::View Hudview;

	sf::RectangleShape Rectangle;

	sf::Texture ship;
	sf::Texture t_shield;
	sf::Texture turret;
	sf::Texture backgroundImage;
	sf::Texture penumbraTexture;
	sf::Texture projectile;
	sf::Texture rocket;
	sf::Texture laser;
	sf::Texture Headsupdisplay;
	sf::Texture exit;
	sf::Texture crystalPT;
	sf::Texture boxCheckTexture;
	//sf::Texture pauseScreenImage;
	sf::Sprite backgroundSprite;
	sf::Sprite playerShipSprite;
	sf::Sprite hovercraftsprite;
	sf::Sprite shieldSprite;
	sf::Sprite exitSprite;
	sf::Sprite playerProjSprite;
	sf::Sprite rocketSprite;
	sf::Sprite laserSprite;
	sf::Sprite headsupSprite;
	sf::Sprite crystalSprite;
	sf::Sprite shieldCheckBox;
	sf::Sprite missileCheckBox;
	sf::Sprite laserCheckBox;
	sf::Shader unshadowShader;
	sf::Shader lightOverShapeShader;
	sf::Music musictrack;
	sf::RectangleShape ending;

	sf::IntRect r1;

	PlayerShip*			o_PlayerShip;
	SpiderLing*			o_Spiderling;
	Prowler*			o_Prowler;
	Shield*				o_Shield;
	PlayerProjectile*	o_Projectile;
	hovercraftturret*	o_Hovercraftturret;
	HeadsUp*			o_HeadsUpDisplay;
	MapLoad*			o_MapLoad;
	SpriteManager*		pSpriteManager;
	RocketLauncher*		o_RocketLauncher;
	LaserCannon*		o_LaserCannon;
	Pause*				o_Pause;
	Exit*				o_Exit;
	CrystalPickup*		o_Crystal;
	SoundManager*		o_Soundmanager;

	sf::Vector2i localMousePosition;

	std::vector<Prowler*> prowlerlist;
	std::vector<SpiderLing*> spiderlinglist;
	std::vector<sf::Vector2f> nestPositionsProwler;
	std::vector<sf::Vector2f> nestPositionsSpider;
	std::vector<sf::Vector2f> crystalPos;
	std::vector<PlayerProjectile*> playerprojV;
	std::vector<RocketLauncher*> rocketprojV;
	std::vector<LaserCannon*> laserprojV;
	std::vector<CrystalPickup*> crystallist;

	//std::vector<const int> tilelist;

	int viewsizeX = 800;
	int viewsizeY = 600;
	bool camerashakecounter = 0;
	bool pause = false;
	bool KeyPressed = false;
	bool lastPressed1 = false;
	bool lastPressed2 = false;
	bool lastPressed3 = false;
	bool pressed1;
	bool pressed2;
	bool pressed3;
	bool activepressed1 = false;
	bool activepressed2 = false;
	bool activepressed3 = false;
	bool shieldPickedUp = false;
	bool rocketLight = false;
	bool mainshotLight = false;
	float elapsedtime;

	ltbl::LightSystem ls;
	sf::Sprite light;
	sf::Sprite bossLight;
	sf::Texture lightTexture;
	sf::RenderTexture lightMapTexture;
	sf::Sprite lightmap;

	const int tileWidth = 270;
	const int tileHeight = 220;
	const int* tiles;
	//std::vector<sf::FloatRect> tileHitboxes;

	//sf::FloatRect tileHitboxes[155][110];
	sf::FloatRect tileHitboxes[270][220];
};