#include "stdafx.h"
#include "cScreen.hpp"

class screen_win : public cScreen
{
private:
	sf::View view1;
	sf::VideoMode vidMode;
	sf::Texture backgroundImage;

public:
	screen_win(void);
	virtual int Run(sf::RenderWindow &App, sf::Clock deltaTime);
};