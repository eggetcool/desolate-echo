#pragma once

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using namespace sf;

//template <typename T>

class MathEx
{

public:

	static float S_ClampValue(float p_Value, float p_Min, float p_Max);
	
	static Vector2f S_ClampVector(Vector2f p_Vector, Vector2f p_Min, Vector2f p_Max);

	static Vector2f S_VectorAbs(Vector2f p_Vector);

	static float S_VectorDistance(Vector2f p_A, Vector2f p_B);

	static float S_VectorMagnitude(Vector2f p_Vector);

	static Vector2f S_VectorNormal(Vector2f p_Vector);

	static Vector2f S_AngledUnitVector(float p_Angle);
	
	static float to_angle(const Vector2f& rhs);

	static int S_RandomInt(int p_RangeMin, int p_RangeMax);

	static float S_RandomFloat(float p_RangeMin, float p_RangeMax);

	static Vector2f S_RandomUnitCircle();

	static Vector2f S_RandomInsideCircle(float p_RandomizeRadius);

private:

	static float S_Rad2Deg();

	static float S_Deg2Rad();
	
};

/*
typedef MathEx <int> MathEx_I;
typedef MathEx <unsigned int> MathEx_U;
typedef MathEx <float> MathEx_F;
*/