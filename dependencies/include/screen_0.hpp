#pragma once
#include <iostream>
#include "stdafx.h"
#include "cScreen.hpp"

class screen_0 : public cScreen
{
public:
	screen_0(void);
	virtual int Run(sf::RenderWindow &App, sf::Clock deltaTime);
private:
	sf::View view1;
	int alpha_max;
	int alpha_div;
	bool playing;

	sf::Texture backgroundImage;
	sf::Texture newGameTexture;
	sf::Texture ResumeGameTexture;
	sf::Texture OptionsMenuTexture;
	sf::Texture ExitTexture;
	sf::Sprite newGame;
	sf::Sprite ResumeGame;
	sf::Sprite Options;
	sf::Sprite Exit;
	sf::Music musictrack;
	sf::Music button;
};