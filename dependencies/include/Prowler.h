#pragma once
#include "SoundManager.h"

class PlayerShip;
class HeadsUp;
class aimAtMouse;
class SpriteManager;
class AnimatedTexture;
class Shield;
class SoundManager;

class Prowler : public sf::Transformable
{
public:
	Prowler(SpriteManager* pSpriteManager, sf::Vector2f nestPos, sf::Vector2f SpritePos, PlayerShip* pShip, HeadsUp* plifemeter, Shield* o_Shield, SoundManager* o_Soundmanager);
	~Prowler();
	void Update(float deltatime, sf::Vector2f SpritePos);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	void Life(float);
	bool IsVisible();
	void Death();
	void Move(float x, float y);
	Vector2f GetVPos();
	void Draw(sf::RenderWindow& App);
	void normalize(sf::Vector2f& rhs);
	void Attack();
	bool isDead();
	sf::FloatRect getHitbox();
	void setPColorRed();
	void setPColorWhite();
	sf::Vector2f GetDir();
	AnimatedTexture* GetTexture();
	void Animation();
	bool IsHit();
	void Hit(bool);
	void Mad(bool);
	bool isMad();

private:
	Prowler() {};

	PlayerShip*			pShip;
	HeadsUp*			lifemeter;
	aimAtMouse*			mouseAim;
	AnimatedTexture*	ProwlerFly;
	AnimatedTexture*	ProwlerAttack;
	AnimatedTexture*	ProwlerDeath;
	//PlayerShip*		pShip;
	//HeadsUp*			lifemeter;
	Shield*				o_Shield;
	SoundManager*		o_Soundmanager;
	//aimAtMouse*		mouseAim;
	
	sf::Vector2f nestPos;
	sf::Vector2f shipPosition;
	sf::Vector2f position;
	sf::Vector2f velocity;
	sf::Vector2f diff;
	sf::Texture m_tprowler;
	sf::Sprite mSpriteSheet;
	sf::Sound* hoverHitSound;
	sf::Sound* hoverShieldHitSound;
	sf::Sound* prowlerDeathSound;
	sf::Sound* prowlerAggroSound;
	sf::FloatRect hitboxProwler;

	float mX;
	float mY;
	float movementSpeed;
	float attackdeltatime;
	float idleafterchasetime;
	float maxspeed = 3.0f;
	float timeElapsed;
	float damage;
	int mScreenWidth;
	int life;
	bool mVisible;
	bool mDead;
	bool dyingforeal = false;
	bool changeAnimation;
	bool hit;
	bool mMad;
};


