#include "stdafx.h"
#include "PlayerShip.h"
#include "aimAtMouse.h"
#include "Shield.h"
#include "SoundManager.h"
#include "SpriteManager.h"
#include "AnimatedTexture.h"
#include "MapLoad.h"

PlayerShip::PlayerShip(SpriteManager* pSpriteManager, int pScreenWidth, SoundManager* o_Soundmanager)
{
	forward = pSpriteManager->CreateAnimatedTexture("../data/hovercraft_forward.png");
	backwards = pSpriteManager->CreateAnimatedTexture("../data/hovercraft_backwards.png");
	right = pSpriteManager->CreateAnimatedTexture("../data/hovercraft_turn_right.png");
	left = pSpriteManager->CreateAnimatedTexture("../data/hovercraft_turn_left.png");
	idle = pSpriteManager->CreateAnimatedTexture("../data/hovercraft_idle.png");
	ShieldRecharge = pSpriteManager->CreateAnimatedTexture("../data/shield_recharged.png");
	ShieldHit = pSpriteManager->CreateAnimatedTexture("../data/shield_hit.png");
	ShieldDepleted = pSpriteManager->CreateAnimatedTexture("../data/shield_depleted.png");

	mScreenWidth = pScreenWidth;
	Animation();
	if (changeAnimation)
	{
		mSprite.setTextureRect(forward->GetRegion());
		mSprite.setTexture(forward->GetTexture());
	}

	if (changeAnimation)
	{
		mSprite.setTextureRect(backwards->GetRegion());
		mSprite.setTexture(backwards->GetTexture());
	}
	if (changeAnimation)
	{
		mSprite.setTextureRect(right->GetRegion());
		mSprite.setTexture(right->GetTexture());
	}

	if (changeAnimation)
	{
		mSprite.setTextureRect(left->GetRegion());
		mSprite.setTexture(left->GetTexture());
	}
	if (changeAnimation)
	{
		mSprite.setTextureRect(idle->GetRegion());
		mSprite.setTexture(idle->GetTexture());
	}
	if (changeAnimation)
	{
		mSpriteSheetShield.setTextureRect(ShieldRecharge->GetRegion());
		mSpriteSheetShield.setTexture(ShieldRecharge->GetTexture());
	}
	if (changeAnimation)
	{
		mSpriteSheetShield.setTextureRect(ShieldHit->GetRegion());
		mSpriteSheetShield.setTexture(ShieldHit->GetTexture());
	}
	if (changeAnimation)
	{
		mSpriteSheetShield.setTextureRect(ShieldDepleted->GetRegion());
		mSpriteSheetShield.setTexture(ShieldDepleted->GetTexture());
	}

	mVisible = true;
	lastPressed = false;
	lastPressedS = false;
	lastPressedRight = false;
	lastPressedLeft = false;
	life = 100.0f;
	position.x = 530 * 8;
	position.y = 770 * 8;
	angle = 0;
	animationTimerhit = 0;
	animationTimer = 0;
	pressed;
	pressedS;
	pressedLeft;
	pressedRight;
	mSprite.setOrigin(sf::Vector2f((mSprite.getGlobalBounds().width / 2), (mSprite.getGlobalBounds().height / 2)+ 3));
	mSpriteSheetShield.setOrigin(sf::Vector2f((mSpriteSheetShield.getGlobalBounds().width / 2), (mSpriteSheetShield.getGlobalBounds().height / 2)));

	hoverDeathSound = o_Soundmanager->CreateSound("../data/Hover_Death.wav");
	hoverDeathSound->setPitch(MathEx::S_RandomFloat(0.9, 1.2));

	hoverThrusterSound = o_Soundmanager->CreateSound("../data/Thrusters.wav");
	hoverThrusterSound->setLoop(true);
	hoverThrusterSound->setPitch(0.8);
	hoverThrusterSound->setVolume(25);

	hoverThrusterSoundBack = o_Soundmanager->CreateSound("../data/Thrusters.wav");
	hoverThrusterSoundBack->setLoop(true);
	hoverThrusterSoundBack->setPitch(0.6);
	hoverThrusterSoundBack->setVolume(15);

	hoverThrusterSoundRight = o_Soundmanager->CreateSound("../data/Thrusters.wav");
	hoverThrusterSoundRight->setLoop(true);
	hoverThrusterSoundRight->setPitch(1);
	hoverThrusterSoundRight->setVolume(10);

	hoverThrusterSoundLeft = o_Soundmanager->CreateSound("../data/Thrusters.wav");
	hoverThrusterSoundLeft->setLoop(true);
	hoverThrusterSoundLeft->setPitch(1);
	hoverThrusterSoundLeft->setVolume(10);
}

PlayerShip::~PlayerShip()
{
	/*delete mCollider;
	mCollider = nullptr;*/
}

void PlayerShip::Update(float deltatime)
{
	//std::cout << position.x << "\t" << position.y << std::endl;
	idle->Update(deltatime);
	mSprite.setTextureRect(idle->GetRegion());
	mSprite.setTexture(idle->GetTexture());

	if (shieldRecharge && animationTimer <= 1.44)
	{
		animationTimer += deltatime;
		ShieldRecharge->Update(deltatime);
		mSpriteSheetShield.setTextureRect(ShieldRecharge->GetRegion());
		mSpriteSheetShield.setTexture(ShieldRecharge->GetTexture());
	}
	if (animationTimer >= 1.44)
	{
		shieldRecharge = false;
		animationTimer = 0;
	}
	if (shieldActive && hit)
	{
		animationTimerhit += deltatime;
		ShieldHit->Update(deltatime);
		mSpriteSheetShield.setTextureRect(ShieldHit->GetRegion());
		mSpriteSheetShield.setTexture(ShieldHit->GetTexture());
		
	}
	if (animationTimerhit >= 0.9)
	{
		hit = false;
		animationTimerhit = 0;
	}
	/*mCollider->SetPosition(position.x, position.y);*/

	hitboxShip = mSprite.getGlobalBounds();
	hitboxShip.top = position.y - mSprite.getScale().x;
	hitboxShip.left = position.x - mSprite.getScale().y;

	pressedLeft = sf::Keyboard::isKeyPressed(sf::Keyboard::A);

	if (pressedLeft && !lastPressedLeft)
	{
		hoverThrusterSoundLeft->play();
	}
	lastPressedLeft = pressedLeft;

	if (!pressedLeft)
	{
		hoverThrusterSoundLeft->stop();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		mSprite.rotate(-2.0f);

		left->Update(deltatime);
		mSprite.setTextureRect(left->GetRegion());
		mSprite.setTexture(left->GetTexture());
	}

	pressedRight = sf::Keyboard::isKeyPressed(sf::Keyboard::D);

	if (pressedRight && !lastPressedRight)
	{
		hoverThrusterSoundRight->play();
	}
	lastPressedRight = pressedRight;

	if (!pressedRight)
	{
		hoverThrusterSoundRight->stop();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		mSprite.rotate(2.0f);

		right->Update(deltatime);
		mSprite.setTextureRect(right->GetRegion());
		mSprite.setTexture(right->GetTexture());
	}

	pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::W);

	if (pressed && !lastPressed)
	{
		hoverThrusterSound->play();
	}
	lastPressed = pressed;

	if (!pressed)
	{
		hoverThrusterSound->stop();
	}

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		float angleRADS = (mSprite.getRotation()) / 180 * M_PI;
		float newPosY = 1.0f*sin(angleRADS - M_PI / 2);
		float newPosX = 1.0f*cos(angleRADS - M_PI / 2);
		direction.x = newPosX;
		direction.y = newPosY;
		angle = mouseAim->to_angle(mouseAim->normalized(direction));
		
		velocity.x += (accel*direction.x);
		velocity.y += (accel*direction.y);

		Move(direction.x, direction.y);
			
		forward->Update(deltatime);
		mSprite.setTextureRect(forward->GetRegion());
		mSprite.setTexture(forward->GetTexture());	
	}

	pressedS = sf::Keyboard::isKeyPressed(sf::Keyboard::S);

	if (pressedS && !lastPressedS)
	{
		hoverThrusterSoundBack->play();
	}
	lastPressedS = pressedS;

	if (!pressedS)
	{
		hoverThrusterSoundBack->stop();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		float angleRADS = (mSprite.getRotation()) / 180 * M_PI;
		float newPosY = 1.0f*sin(angleRADS + M_PI / 2);
		float newPosX = 1.0f*cos(angleRADS + M_PI / 2);
		direction.x = newPosX;
		direction.y = newPosY;
		float angle = mouseAim->to_angle(mouseAim->normalized(direction));

		velocity.x += (accelB*direction.x);
		velocity.y += (accelB*direction.y);

		Move(direction.x, direction.y);

		backwards->Update(deltatime);
		mSprite.setTextureRect(backwards->GetRegion());
		mSprite.setTexture(backwards->GetTexture());
	}

	velocity.x *= decel;
	velocity.y *= decel;

	if (velocity.x < -maxspeed) velocity.x = -maxspeed;
	if (velocity.x > maxspeed) velocity.x = maxspeed;
	if (velocity.y < -maxspeed) velocity.y = -maxspeed;
	if (velocity.y > maxspeed) velocity.y = maxspeed;

	position += velocity;
	mSprite.setPosition(position);
	mSpriteSheetShield.setPosition(position);
}

void PlayerShip::Move(float x, float y)
{
	position.x += x;
	position.y += y;
}

void PlayerShip::Hit(sf::Vector2f enemyPos)
{
	animationTimerhit = 0;
	hit = true;
	//knockback
	Vector2f diff;
	diff.x = ((position.x) - enemyPos.x);
	diff.y = ((position.y)- enemyPos.y);
	float angle = mouseAim->to_angle(mouseAim->normalized(diff));

	velocity.x += (accel*(diff.x));
	velocity.y += (accel*(diff.y));

	Move(5*accel*diff.x, 5*accel*diff.y);
	//mSprite.setPosition(position);
}

sf::Vector2f PlayerShip::GetVPos()
{
	return position;
}

sf::Sprite PlayerShip::GetSprite()
{
	return mSprite;
}

float PlayerShip::GetX()
{
	return position.x;
	
}

float PlayerShip::GetY()
{
	return position.y;
}

int PlayerShip::Life(int pLife)
{
	life += pLife;
	//std::cout << life << endl;
	/*if (life < 50)
	{
		NearDeath();
		return 1;
	}*/
	if (life <= 0)
	{
		Death();
		return 0;
	}
	if (life >= 100)
	{
		life = 100;
		return 1;
	}
	return 1;
}

bool PlayerShip::IsVisible()
{
	return mVisible;
}

void PlayerShip::NearDeath()
{
	//Change sprite
}

void PlayerShip::Death()
{
	//Change sprite
	life = 0;
	hoverDeathSound->play();
}

void PlayerShip::SetVPos(sf::Vector2f setPosition)
{
	position = setPosition;
}

float PlayerShip::GetDir()
{
	return angle;
}

void PlayerShip::Draw(sf::RenderWindow & App)
{
	App.draw(mSprite);
	if (animationTimer > 0 || animationTimerhit > 0)
		App.draw(mSpriteSheetShield);
	/*if (hit)
		App.draw(mSpriteSheetShield);*/
}

sf::FloatRect PlayerShip::getHitbox()
{
	return hitboxShip;
}

//Collider* PlayerShip::GetCollider()
//{
//	return mCollider;
//}

float PlayerShip::GetLife()
{
	return life;
}

void PlayerShip::shieldPickedUp(bool value)
{
	shieldPicked = value;
	shieldActive = value;
	shieldRecharge = value;
}

bool PlayerShip::isShieldPicked()
{
	return shieldPicked;
}

void PlayerShip::Animation()
{
	float framefloat = 0.06;

	if (changeAnimation)
	{
		forward->AddFrame(0, 0, 23, 40, framefloat);
		forward->AddFrame(33, 0, 23, 40, framefloat);
	}

	if (changeAnimation)
	{
		backwards->AddFrame(0, 0, 23, 36, framefloat);
		backwards->AddFrame(33, 0, 23, 36, framefloat);
	}
	if (changeAnimation)
	{
		right->AddFrame(0, 0, 23, 40, framefloat);
		right->AddFrame(33, 0, 23, 40, framefloat);
	}

	if (changeAnimation)
	{
		left->AddFrame(0, 0, 23, 40, framefloat);
		left->AddFrame(33, 0, 23, 40, framefloat);
	}

	if (changeAnimation)
	{
		idle->AddFrame(0, 0, 23, 36, framefloat);
	}

	if (changeAnimation)
	{
		ShieldRecharge->AddFrame(0, 0, 64, 64, framefloat);
		ShieldRecharge->AddFrame(65, 0, 64, 64, framefloat);
		ShieldRecharge->AddFrame(130, 0, 64, 64, framefloat);
		ShieldRecharge->AddFrame(0, 65, 64, 64, framefloat);
		ShieldRecharge->AddFrame(65, 65, 64, 64, framefloat);
		ShieldRecharge->AddFrame(0, 130, 64, 64, framefloat);
		ShieldRecharge->AddFrame(65, 130, 64, 64, framefloat);
		ShieldRecharge->AddFrame(130, 65, 64, 64, framefloat);
		ShieldRecharge->AddFrame(130, 130, 64, 64, framefloat);
		ShieldRecharge->AddFrame(195, 0, 64, 64, framefloat);
		ShieldRecharge->AddFrame(195, 65, 64, 64, framefloat);
		ShieldRecharge->AddFrame(260, 0, 64, 64, framefloat);
		ShieldRecharge->AddFrame(195, 130, 64, 64, framefloat);
		ShieldRecharge->AddFrame(260, 65, 64, 64, framefloat);
		ShieldRecharge->AddFrame(325, 0, 64, 64, framefloat);
		ShieldRecharge->AddFrame(390, 0, 64, 64, framefloat);
		ShieldRecharge->AddFrame(325, 65, 64, 64, framefloat);
		ShieldRecharge->AddFrame(260, 130, 64, 64, framefloat);
		ShieldRecharge->AddFrame(325, 130, 64, 64, framefloat);
		ShieldRecharge->AddFrame(390, 65, 64, 64, framefloat);
		ShieldRecharge->AddFrame(390, 130, 64, 64, framefloat);
		ShieldRecharge->AddFrame(0, 195, 64, 64, framefloat);
		ShieldRecharge->AddFrame(0, 260, 64, 64, framefloat);
		ShieldRecharge->AddFrame(65, 195, 64, 64, framefloat);
	}

	if (changeAnimation)
	{
		ShieldHit->AddFrame(0, 0, 64, 64, framefloat);
		ShieldHit->AddFrame(65, 0, 64, 64, framefloat);
		ShieldHit->AddFrame(130, 0, 64, 64, framefloat);
		ShieldHit->AddFrame(0, 65, 64, 64, framefloat);
		ShieldHit->AddFrame(65, 65, 64, 64, framefloat);
		ShieldHit->AddFrame(0, 130, 64, 64, framefloat);
		ShieldHit->AddFrame(65, 130, 64, 64, framefloat);
		ShieldHit->AddFrame(130, 65, 64, 64, framefloat);
		ShieldHit->AddFrame(130, 130, 64, 64, framefloat);
		ShieldHit->AddFrame(195, 0, 64, 64, framefloat);
		ShieldHit->AddFrame(195, 65, 64, 64, framefloat);
		ShieldHit->AddFrame(260, 0, 64, 64, framefloat);
		ShieldHit->AddFrame(195, 130, 64, 64, framefloat);
		ShieldHit->AddFrame(260, 65, 64, 64, framefloat);
		ShieldHit->AddFrame(325, 0, 64, 64, framefloat);
	}

	if (changeAnimation)
	{
		ShieldDepleted->AddFrame(0, 0, 64, 64, framefloat);
		ShieldDepleted->AddFrame(65, 0, 64, 64, framefloat);
		ShieldDepleted->AddFrame(130, 0, 64, 64, framefloat);
		ShieldDepleted->AddFrame(0, 65, 64, 64, framefloat);
		ShieldDepleted->AddFrame(65, 65, 64, 64, framefloat);
		ShieldDepleted->AddFrame(130, 65, 64, 64, framefloat);
		ShieldDepleted->AddFrame(195, 0, 64, 64, framefloat);
		ShieldDepleted->AddFrame(260, 0, 64, 64, framefloat);
		ShieldDepleted->AddFrame(195, 65, 64, 64, framefloat);
		ShieldDepleted->AddFrame(260, 65, 64, 64, framefloat);
		ShieldDepleted->AddFrame(325, 0, 64, 64, framefloat);
		ShieldDepleted->AddFrame(325, 65, 64, 64, framefloat);
	}
}

void PlayerShip::stopSounds()
{
	hoverThrusterSound->stop();
	hoverDeathSound->stop();
	hoverThrusterSoundBack->stop();
	hoverThrusterSoundLeft->stop();
	hoverThrusterSoundRight->stop();
}
