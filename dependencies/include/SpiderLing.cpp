#include "stdafx.h"
#include "SpiderLing.h"
#include "PlayerShip.h"
#include "HeadsUp.h"
#include "aimAtMouse.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "SoundManager.h"
#include "Shield.h"

SpiderLing::SpiderLing(SpriteManager* pSpriteManager, sf::Vector2f nestPos, sf::Vector2f SpritePos, PlayerShip* pShip, HeadsUp* plifemeter, Shield* o_pShield, SoundManager* o_Soundmanager)
{
	SpiderRun = pSpriteManager->CreateAnimatedTexture("../data/spyderling_walk.png");
	SpiderDeath = pSpriteManager->CreateAnimatedTexture("../data/spyderling_death.png");

	Animation();
	if (changeAnimation)
	{
		mSpriteSheet.setTextureRect(SpiderRun->GetRegion());
		mSpriteSheet.setTexture(SpiderRun->GetTexture());
	}

	if (changeAnimation)
	{
		mSpriteSheet.setTextureRect(SpiderDeath->GetRegion());
		mSpriteSheet.setTexture(SpiderDeath->GetTexture());
	}

	life = 300;
	mVisible = true;
	mDead = false;
	dyingforeal = false;
	this->nestPos = nestPos;
	position = nestPos;
	attackdeltatime = 0;
	timeElapsed = 0;
	mSpriteSheet.setPosition(position);

	this->pShip = pShip;
	o_Shield = o_pShield;
	lifemeter = plifemeter;

	shipPosition = SpritePos;
	movementSpeed = (MathEx::S_RandomFloat(2, 3));

	mSpriteSheet.setOrigin(sf::Vector2f(mSpriteSheet.getLocalBounds().width / 4, mSpriteSheet.getLocalBounds().height / 4));

	hoverHitSound = o_Soundmanager->CreateSound("../data/Hover_Hit2.wav");
	hoverHitSound->setPitch(MathEx::S_RandomFloat(0.6, 0.8));

	hoverShieldHitSound = o_Soundmanager->CreateSound("../data/Hover_Shield_Hit.wav");
	hoverShieldHitSound->setPitch(MathEx::S_RandomFloat(0.8, 1.1));

	spiderlingDeathSound = o_Soundmanager->CreateSound("../data/Spider_Death.wav");
	spiderlingDeathSound->setPitch(MathEx::S_RandomFloat(0.8, 1.2));
}

SpiderLing::~SpiderLing()
{
}

void SpiderLing::Update(float deltatime, sf::Vector2f SpritePos)
{
	hitboxSpider = mSpriteSheet.getLocalBounds();
	hitboxSpider.top = position.y - mSpriteSheet.getScale().y;
	hitboxSpider.left = position.x - mSpriteSheet.getScale().x;
	hitboxSpider.height = mSpriteSheet.getTextureRect().height;
	hitboxSpider.width = mSpriteSheet.getTextureRect().width;

	setPosition(position);

	attackdeltatime += deltatime;
	shipPosition = SpritePos;

	sf::Vector2f shipDirection = shipPosition - mSpriteSheet.getPosition();
	float length = sqrt(pow(shipDirection.x, 2) + pow(shipDirection.y, 2));

	if (life <= 0)
	{
		timeElapsed += deltatime;
		if (timeElapsed <= 1.328f)
		{
			dyingforeal = true;
			SpiderDeath->Update(deltatime);
			mSpriteSheet.setTextureRect(SpiderDeath->GetRegion());
			mSpriteSheet.setTexture(SpiderDeath->GetTexture());
		}
		else
		{
			Death();
		}
	}

	if (length > 60 && !dyingforeal)
	{
		SpiderRun->Update(deltatime);
		mSpriteSheet.setTextureRect(SpiderRun->GetRegion());
		mSpriteSheet.setTexture(SpiderRun->GetTexture());
	}
	if (length < 400 && !dyingforeal)
	{
		normalize(shipDirection);
		position += shipDirection * movementSpeed;
		diff.x = SpritePos.x - position.x;
		diff.y = SpritePos.y - position.y;
		float angle = mouseAim->to_angle(mouseAim->normalized(diff));
		mSpriteSheet.setRotation(angle - 90);
		mSpriteSheet.setPosition(position);

		if (getHitbox().intersects(pShip->getHitbox()))
		{
			if (pShip && lifemeter != nullptr && mVisible == true)
			{
				Attack();
			}

			if (mVisible == false && attackdeltatime >= 0.4f)
			{
				attackdeltatime = 0;
				mVisible = true;
			}
		}
		//std::cout << attackdeltatime << std::endl;
	}

	else
	{
		//Fix idle..
		idleafterchasetime += deltatime;
		if (idleafterchasetime > 3)
		{
			sf::Vector2f centerDirection = sf::Vector2f(nestPos) - mSpriteSheet.getPosition();
			length = sqrt(pow(centerDirection.x, 2) + pow(centerDirection.y, 2));

			if (length > 5)
			{
				normalize(centerDirection);
				position += centerDirection * movementSpeed;
				mSpriteSheet.setPosition(position);
			}
		}
	}
	if (life <= 0)
	{
		timeElapsed += deltatime;
		if (timeElapsed <= 1.328)
		{
			if (timeElapsed < 0.5)
			{
				spiderlingDeathSound->play();
			}
			dyingforeal = true;
			SpiderDeath->Update(deltatime);
			mSpriteSheet.setTextureRect(SpiderDeath->GetRegion());
			mSpriteSheet.setTexture(SpiderDeath->GetTexture());
		}
		else
		{
			Death();
		}
	}
}

sf::Sprite SpiderLing::GetSprite()
{
	return mSpriteSheet;
}

float SpiderLing::GetX()
{
	return position.x;
}

float SpiderLing::GetY()
{
	return position.y;
}

void SpiderLing::Life(int pLife)
{
	life += pLife;
}

bool SpiderLing::IsVisible()
{
	return mVisible;
}

void SpiderLing::Death()
{
	mDead = true;
}

void SpiderLing::Move(float x, float y)
{
}

Vector2f SpiderLing::GetVPos()
{
	return position;
}

void SpiderLing::Draw(sf::RenderWindow & App)
{
	App.draw(mSpriteSheet);
}

void SpiderLing::normalize(sf::Vector2f & rhs)
{
	float len = sqrt(pow(rhs.x, 2) + pow(rhs.y, 2));
	if (len > 0.0f)
	{
		rhs.x /= len;
		rhs.y /= len;
	}
}

void SpiderLing::Attack()
{
	if (!o_Shield->isActive())
	{
		pShip->Hit(position);
		pShip->Life(-5);
		lifemeter->GotHit(5.f);
		mVisible = false;

		hoverHitSound->play();
	}
	else
	{
		pShip->Hit(position);
		lifemeter->GotHit(24.16f);
		mVisible = false;

		hoverShieldHitSound->play();
	}
}

bool SpiderLing::isDead()
{
	return mDead;
}

sf::FloatRect SpiderLing::getHitbox()
{
	return hitboxSpider;
}

void SpiderLing::setPScale()
{
	if (mSpriteSheet.getScale().x == 1)
	{
		mSpriteSheet.setScale(1.1 * mSpriteSheet.getScale().x, 1.1 * mSpriteSheet.getScale().y);
	}

	else
		mSpriteSheet.setScale(1, 1);
}

void SpiderLing::setPColorRed()
{
	mSpriteSheet.setColor(sf::Color::Red);
}

void SpiderLing::setPColorWhite()
{
	mSpriteSheet.setColor(sf::Color::White);
}

sf::Vector2f SpiderLing::GetDir()
{
	return diff;
}

AnimatedTexture* SpiderLing::GetTexture()
{
	return SpiderRun;
}

void SpiderLing::Animation()
{
	float framefloat = 0.083;
	if (changeAnimation)
	{
		SpiderRun->AddFrame(0, 0, 32, 32, framefloat);
		SpiderRun->AddFrame(33, 0, 32, 32, framefloat);
		SpiderRun->AddFrame(66, 0, 32, 32, framefloat);
		SpiderRun->AddFrame(0, 33, 32, 32, framefloat);
	}
	if (changeAnimation)
	{
		SpiderDeath->AddFrame(80, 10, 64, 64, framefloat);
		SpiderDeath->AddFrame(145, 10, 64, 64, framefloat);
		SpiderDeath->AddFrame(15, 75, 64, 64, framefloat);
		SpiderDeath->AddFrame(80, 75, 64, 64, framefloat);
		SpiderDeath->AddFrame(15, 140, 64, 64, framefloat);
		SpiderDeath->AddFrame(80, 140, 64, 64, framefloat);
		SpiderDeath->AddFrame(145, 75, 64, 64, framefloat);
		SpiderDeath->AddFrame(145, 140, 64, 64, framefloat);
		SpiderDeath->AddFrame(210, 10, 64, 64, framefloat);
		SpiderDeath->AddFrame(210, 75, 64, 64, framefloat);
		SpiderDeath->AddFrame(275, 10, 64, 64, framefloat);
		SpiderDeath->AddFrame(210, 140, 64, 64, framefloat);
		SpiderDeath->AddFrame(275, 75, 64, 64, framefloat);
		SpiderDeath->AddFrame(340, 10, 64, 64, framefloat);
		SpiderDeath->AddFrame(405, 10, 64, 64, framefloat);
		SpiderDeath->AddFrame(15, 10, 64, 64, framefloat);
	}
}

bool SpiderLing::IsHit()
{
	return hit;
}

void SpiderLing::Hit(bool gotHit)
{
	hit = gotHit;
}

bool SpiderLing::Dying()
{
	return dyingforeal;
}
