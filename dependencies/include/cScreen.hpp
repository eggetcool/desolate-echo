#pragma once
#include "stdafx.h"

class cScreen
{
public:
	virtual int Run(sf::RenderWindow &App, sf::Clock deltaTime) = 0;
};