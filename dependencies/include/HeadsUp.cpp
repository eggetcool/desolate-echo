#include "stdafx.h"
#include "HeadsUp.h"
#include "Shield.h"
#include "SpriteManager.h"
#include "AnimatedTexture.h"
#include "PlayerShip.h"

HeadsUp::HeadsUp(SpriteManager* pSpriteManager, sf::Sprite pSprite, int pScreenWidth, int pScreenHeight, PlayerShip* o_PlayerShip, Shield* o_pshield)
{
	
	ShieldMeterRecharged = pSpriteManager->CreateAnimatedTexture("../data/shieldbar_charging.png");
	shieldMeterAnimation = pSpriteManager->CreateAnimatedTexture("../data/shieldbar_full.png");
	Animation();
	sSprite.setTextureRect(ShieldMeterRecharged->GetRegion());
	sSprite.setTexture(ShieldMeterRecharged->GetTexture());

	this->o_PlayerShip = o_PlayerShip;

	mSprite = pSprite;
	mScreenWidth = pScreenWidth;
	mScreenHeight = pScreenHeight;
	mshipPos = o_PlayerShip->GetVPos();
	life = 100;
	shield = 145;
	timeElapsed = 0;
	rechargeTimer = 0;
	noHitTimer = 0;
	rechargeAnimationTimer = 0;
	currentlife = life;
	currentshield = shield;
	crystalPoints = 0;
	o_shield = o_pshield;
	gothit = false;
	rechargeBool = false;

	//lifemeter.setSize(sf::Vector2f(65, 20));

	lifemeter.left = 0;
	lifemeter.top = 0;
	lifemeter.width = 1;
	lifemeter.height = 10;
	lifemetertext.loadFromFile("../data/healthbar.png");

	//Anv�nd denna box f�r att rita ut delar av animationen.
	shieldmeter.left = 0;
	shieldmeter.top = 0;
	shieldmeter.width = 41;
	shieldmeter.height = 0;

	hSprite.setTextureRect(lifemeter);
	hSprite.setTexture(lifemetertext);
	hSprite.setColor(sf::Color::Green);

	
	//sSprite.setTextureRect(shieldmeter);
	//sSprite.setTexture(shieldmetertext);
	//sSprite.setColor(sf::Color(0, 200, 248));
	
	mSprite.setScale(3.125, 3.125);
	hSprite.setScale(currentlife, 3.125f);
	//sSprite.setScale(currentshield, 3.125f);
}

HeadsUp::~HeadsUp()
{

}

void HeadsUp::Update(float deltatime, Vector2f hudPos)
{
	mHudPos = hudPos;

	hSprite.setPosition(mHudPos.x - 343, mHudPos.y + 216);
	mSprite.setPosition(mHudPos.x - 402, mHudPos.y + 237 - mSprite.getLocalBounds().height);
	sSprite.setPosition(mHudPos.x - 350, mHudPos.y + 263);
	sSprite.setScale(3.125f, 3.125f);

	
	//L�gg till idle full animation
	if (o_shield->isPickedUp())
	{
		rechargeAnimationTimer += deltatime;
		if (rechargeAnimationTimer <= 1.f && changetheAnimation)
		{
			if (rechargeAnimationTimer == 1.f)
			{
				o_shield->playShieldChargingSound();
			}

			ShieldMeterRecharged->Update(deltatime);
			sSprite.setTextureRect(ShieldMeterRecharged->GetRegion());
			sSprite.setTexture(ShieldMeterRecharged->GetTexture());
		}
		else
		{
			sf::IntRect shieldRegion(shieldMeterAnimation->GetRegion().left, shieldMeterAnimation->GetRegion().top, shieldmeter.width, shieldMeterAnimation->GetRegion().height);
			shieldMeterAnimation->Update(deltatime);

			sSprite.setTextureRect(shieldRegion);
			sSprite.setTexture(shieldMeterAnimation->GetTexture());
			changetheAnimation = false;
			rechargeAnimationTimer = 0;
		}

	}

	if (IsHit())
	{
		timeElapsed += deltatime;
		rechargeBool = true;
		noHitTimer = 0;
		if (timeElapsed >= 0.05 && o_shield->isActive())
		{
			sSprite.setColor(sf::Color::White);
			timeElapsed = 0;
			Hit(false);
		}
		else if (timeElapsed >= 0.05 && !o_shield->isActive())
		{
			hSprite.setColor(sf::Color::Green);
			timeElapsed = 0;
			Hit(false);
		}
	}
	if (currentshield < 145)
	{
		if (rechargeBool)
		{
			noHitTimer += deltatime;
			if (noHitTimer > 8)
			{
				changetheAnimation = true;
				noHitTimer = 0;
				rechargeBool = false;
				o_shield->Activate(true);
				o_shield->PickUp(true);
				o_PlayerShip->shieldPickedUp(true);
				currentshield = shield;
				
				//sSprite.setScale(currentshield, 3.125f);
			}
		}
	}
	shieldmeter.width = currentshield / 3.125f;

}

sf::Sprite HeadsUp::GetSprite()
{
	return mSprite;
}

float HeadsUp::GetX()
{
	return mshipPos.x;
}

float HeadsUp::GetY()
{
	return mshipPos.y;
}

void HeadsUp::GotHit(float damage)
{
	if (o_shield->isPickedUp())
	{
		currentshield -= damage;
		

		sSprite.setColor(sf::Color(222, 222, 222));
		gothit = true;

		std::cout << "shield" << std::endl;
		if (currentshield <= 0)
		{
			o_shield->playShieldDepletedSound();
			o_shield->Activate(false);
			o_shield->PickUp(false);
			o_PlayerShip->shieldPickedUp(false);
		}
	}
	else
	{
		currentlife -= damage;
		hSprite.setScale(currentlife, 3.125f);
		hSprite.setColor(sf::Color::Red);
		gothit = true;

		std::cout << "life" << std::endl;
	}
}

void HeadsUp::GotLife(float heal)
{
	gothit = false;
	currentlife += heal;
	hSprite.setScale(currentlife, 3.125f);
}

void HeadsUp::GotShield()
{
	gothit = false;
	
	currentshield = shield;
	//sSprite.setScale(currentshield, 3.125f);
	o_shield->Activate(1);
	o_shield->PickUp(1);
	
}

void HeadsUp::AddCrystal()
{
	crystalPoints += 1;
	std::cout  << "Crystals Obtained: "<< crystalPoints << std::endl;
}

void HeadsUp::RemoveCrystal(int crystalRemoved)
{
	crystalPoints -= crystalRemoved;
	std::cout << "Crystals Obtained: " << crystalPoints << std::endl;
}

int HeadsUp::howManyCrystals()
{
	return crystalPoints;
}

void HeadsUp::Hit(bool hit)
{
	gothit = hit;
}

bool HeadsUp::IsHit()
{
	return gothit;
}

void HeadsUp::Draw(sf::RenderWindow & App)
{
	if (o_shield->isPickedUp())
	{
		App.draw(sSprite);
	}
	App.draw(hSprite);
	App.draw(mSprite);
}

void HeadsUp::Animation()
{
	float framefloat = 0.05;

	ShieldMeterRecharged->AddFrame(0, 0, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(0, 11, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(0, 22, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(0, 33, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(0, 44, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(42, 0, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(42, 11, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(42, 22, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(42, 33, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(84, 0, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(42, 44, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(84, 11, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(84, 22, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(84, 33, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(84, 44, 41, 10, framefloat);
	ShieldMeterRecharged->AddFrame(0, 55, 41, 10, framefloat);

	
	shieldMeterAnimation->AddFrame(0, 0, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(0, 11, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(0, 22, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(0, 33, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(0, 44, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(42, 0, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(42, 11, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(42, 22, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(42, 33, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(84, 0, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(42, 44, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(84, 11, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(84, 22, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(84, 33, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(84, 44, 41, 10, framefloat);
	shieldMeterAnimation->AddFrame(0, 55, 41, 10, framefloat);

}
