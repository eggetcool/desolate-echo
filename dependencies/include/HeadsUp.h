#pragma once

class Shield;
class SpriteManager;
class AnimatedTexture;
class PlayerShip;

class HeadsUp
{
public:
	HeadsUp(SpriteManager* pSpriteManager, sf::Sprite pSprite, int pScreenWidth, int pScreenHeight, PlayerShip* o_PlayerShip, Shield* o_pshield);
	~HeadsUp();
	void Update(float deltatime, Vector2f shipPos);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	void GotHit(float damage);
	void GotLife(float heal);
	void GotShield();
	void AddCrystal();
	void RemoveCrystal(int);
	int howManyCrystals();
	void Hit(bool);
	bool IsHit();
	void Draw(sf::RenderWindow& App);
	void Animation();

private:
	HeadsUp() {};
	sf::Sprite		 mSprite;
	sf::Sprite		 hSprite;
	sf::Sprite		 sSprite;
	sf::Sprite		 mSpriteSheet;
	sf::IntRect		 lifemeter;
	sf::IntRect		 shieldmeter;
	sf::Texture		 lifemetertext;
	sf::Texture		 shieldmetertext;
	Vector2f		 mshipPos;
	Vector2f		 mHudPos;
	
	AnimatedTexture* ShieldMeterRecharged;
	AnimatedTexture* shieldMeterAnimation;
	Shield*			 o_shield;
	PlayerShip*		 o_PlayerShip;

	float life;
	float shield;
	float mX;
	float mY;
	float timeElapsed;
	float rechargeTimer;
	float noHitTimer;
	float rechargeAnimationTimer;
	int mScreenWidth;
	int mScreenHeight;
	int currentlife;
	int currentshield;
	int crystalPoints;
	bool gothit;
	bool rechargeBool;
	bool changetheAnimation = true;
};