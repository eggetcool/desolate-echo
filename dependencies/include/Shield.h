#pragma once
#include "SoundManager.h"

class PlayerShip;
class HeadsUp;
class SoundManager;

class Shield : public sf::Transformable
{
public:
	Shield(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f SpritePos, PlayerShip* o_PlayerShip, HeadsUp* o_HeadsUpDisplay, SoundManager* o_Soundmanager);
	~Shield();
	void Update(float deltatime, sf::Vector2f SpritePos);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	bool IsVisible();
	void Disappear();
	void PickUp(bool);
	bool isPickedUp();
	void Activate(bool);
	bool isActive();
	void Deactivate(bool);
	bool isDeactive();
	void playShieldDepletedSound();
	void playShieldChargedSound();
	void playShieldChargingSound();
	Vector2f GetVPos();
	void Draw(sf::RenderWindow& App);
	sf::FloatRect getHitbox();

private:
	Shield() {};

	bool pickUp;
	bool active;
	bool deactive;
	bool mVisible;
	float mX;
	float mY;
	int mScreenWidth;

	sf::Sprite mSprite;
	sf::Sprite mSpriteSheet;
	sf::Vector2f shipPosition;
	sf::Vector2f position;
	sf::FloatRect hitboxShield;
	sf::Sound* shieldChargedSound;
	sf::Sound* shieldDepletedSound;
	sf::Sound* shieldChargingSound;

	PlayerShip*			o_PlayerShip;
	HeadsUp*			o_HeadsUpDisplay;
	SoundManager*		o_Soundmanager;
	

};

