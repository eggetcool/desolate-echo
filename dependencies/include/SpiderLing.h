#pragma once
#include "SoundManager.h"

class PlayerShip;
class HeadsUp;
class Shield;
class aimAtMouse;
class SpriteManager;
class AnimatedTexture;
class Shield;

class SpiderLing : public sf::Transformable
{
public:
	SpiderLing(SpriteManager* pSpriteManager, sf::Vector2f nestPos, sf::Vector2f SpritePos, PlayerShip* pShip, HeadsUp* plifemeter, Shield* o_pShield, SoundManager* o_Soundmanager);
	~SpiderLing();
	void Update(float deltatime, sf::Vector2f SpritePos);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	void Life(int);
	bool IsVisible();
	void Death();
	void Move(float x, float y);
	Vector2f GetVPos();
	void Draw(sf::RenderWindow& App);
	void normalize(sf::Vector2f& rhs);
	void Attack();
	bool isDead();
	sf::FloatRect getHitbox();
	void setPScale();
	void setPColorRed();
	void setPColorWhite();
	sf::Vector2f GetDir();
	AnimatedTexture* GetTexture();
	void Animation();
	bool IsHit();
	void Hit(bool);
	void playHoverHitSound();
	void playShieldhitSound();
	void playSpiderDeathSound();
	bool Dying();
	SoundManager* o_Soundmanager;

private:
	SpiderLing() {};

	PlayerShip*		 pShip;
	HeadsUp*		 lifemeter;
	aimAtMouse*		 mouseAim;
	AnimatedTexture* SpiderRun;
	AnimatedTexture* SpiderDeath;
	Shield*			 o_Shield;

	float mX;
	float mY;
	float movementSpeed;
	float attackdeltatime;
	float idleafterchasetime;
	float maxspeed = 5.0f;
	float timeElapsed;
	int mScreenWidth;
	int life;
	bool mVisible;
	bool mDead;
	bool dyingforeal;
	bool changeAnimation;
	bool hit;

	sf::Vector2f nestPos;
	sf::Vector2f shipPosition;
	sf::Vector2f position;
	sf::Vector2f velocity;
	sf::Vector2f diff;
	sf::Texture m_tprowler;
	sf::Sprite mSpriteSheet;

	sf::Sound* hoverHitSound;
	sf::Sound* hoverShieldHitSound;
	sf::Sound* spiderlingDeathSound;

	sf::FloatRect hitboxSpider;
};