#pragma once

#include "Sprite.h"

class AnimatedTexture
{
public:
	struct FrameData
	{
		sf::IntRect m_xRegion;
		float m_fDuration;
		//sf::Texture m_pxTexture;
	};
	AnimatedTexture(sf::Texture p_xTexture);
	void AddFrame(int p_iX, int p_iY, int p_iWidth, int p_iHeight, float p_fDuration);
	void Update(float p_fDeltatime);
	sf::Texture& GetTexture();
	sf::IntRect GetRegion();
private:
	std::string m_sFilepath;
	sf::Texture m_xTexture;
	std::vector<FrameData> m_axFrames;
	sf::IntRect m_xRegion;
	int m_iIndex;
	float m_fCurrentDuration;

};
