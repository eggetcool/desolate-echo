#pragma once
#include "aimAtMouse.h"

class LaserCannon
{
public:
	LaserCannon(sf::Sprite pSprite, Vector2f position, Vector2f direction, int viewsizeX, int viewsizeY);
	~LaserCannon();
	void Update(float deltatime, Vector2i mousePos);
	sf::Sprite GetSprite();
	bool IsVisible();
	bool IsActive();
	void Deactivate();
	void Activate();
	void playActivateSound();
	void Move(float x, float y);
	Vector2f GetVPos();
	std::vector<LaserCannon*> LaserLightV;

	sf::Sprite GetLaserLight();

	sf::FloatRect getHitbox();

private:
	LaserCannon() {};
	sf::Sprite mSprite;

	bool mVisible;
	bool mActive;
	sf::Event Event;
	sf::Vector2f position;
	sf::Vector2f direction;
	aimAtMouse* mouseAim;

	sf::Sprite LaserLight;
	sf::Texture LaserLightTexture;

	float maxspeed;
	float currentSpeed;
	float accel;
	int width;
	int height;

	sf::FloatRect hitboxLaser;
};
