#include "stdafx.h"
#include "ChrystalPickup.h"
#include "PlayerShip.h"
#include "HeadsUp.h"
#include "SoundManager.h"

CrystalPickup::CrystalPickup(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f crystalPos, PlayerShip* o_PlayerShip, HeadsUp* o_HeadsUpDisplay, SoundManager* o_Soundmanager)
{
	mSprite = pSprite;
	mScreenWidth = pScreenWidth;
	position = crystalPos;
	this->o_PlayerShip = o_PlayerShip;
	this->o_HeadsUpDisplay = o_HeadsUpDisplay;
	shipPosition = o_PlayerShip->GetVPos();
	active = true;
	pickUp = false;

	mSprite.setOrigin(mSprite.getGlobalBounds().width / 2, mSprite.getGlobalBounds().height / 2);

	crystalPickupSound = o_Soundmanager->CreateSound("../data/Crystal.wav");
}

CrystalPickup::~CrystalPickup()
{
}

void CrystalPickup::Update(float deltatime)
{

	hitboxCrystal = mSprite.getGlobalBounds();
	hitboxCrystal.left = position.x;
	hitboxCrystal.top = position.y;
	shipPosition = o_PlayerShip->GetVPos();

	if (o_PlayerShip->getHitbox().intersects(getHitbox()))
	{
		Disappear();
		o_HeadsUpDisplay->AddCrystal();
		crystalPickupSound->play();
	}

	mSprite.setPosition(position);
}

sf::Sprite CrystalPickup::GetSprite()
{
	return mSprite;
}

float CrystalPickup::GetX()
{
	return position.x;
}

float CrystalPickup::GetY()
{
	return position.y;
}

bool CrystalPickup::IsVisible()
{
	return mVisible;
}

void CrystalPickup::Disappear()
{
	mVisible = false;
	active = false;
}

void CrystalPickup::PickUp(bool pPickUp)
{
	pickUp = pPickUp;
}

bool CrystalPickup::isPickedUp()
{
	return pickUp;
}

void CrystalPickup::Activate(bool pActive)
{
	active = pActive;
}

bool CrystalPickup::isActive()
{
	return active;
}

Vector2f CrystalPickup::GetVPos()
{
	return position;
}

void CrystalPickup::Draw(sf::RenderWindow & App)
{
	if (mVisible)
	{
		App.draw(mSprite);
	}
}

sf::FloatRect CrystalPickup::getHitbox()
{
	return hitboxCrystal;
}