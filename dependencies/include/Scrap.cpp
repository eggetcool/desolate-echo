#include "stdafx.h"
#include "PlayerShip.h"
#include "HeadsUp.h"
#include "SoundManager.h"
#include "Scrap.h"

Scrap::Scrap(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f crystalPos, PlayerShip * o_PlayerShip, HeadsUp * o_HeadsUpDisplay, SoundManager* o_Soundmanager)
{
	mSprite = pSprite;
	mScreenWidth = pScreenWidth;
	position = crystalPos;
	this->o_PlayerShip = o_PlayerShip;
	this->o_HeadsUpDisplay = o_HeadsUpDisplay;
	shipPosition = o_PlayerShip->GetVPos();
	active = true;
	pickUp = false;

	mSprite.setOrigin(mSprite.getGlobalBounds().width / 2, mSprite.getGlobalBounds().height / 2);
}

Scrap::~Scrap()
{
}

void Scrap::Update(float deltatime)
{
	hitboxScrap = mSprite.getGlobalBounds();
	hitboxScrap.left = position.x;
	hitboxScrap.top = position.y;
	shipPosition = o_PlayerShip->GetVPos();

	if (o_PlayerShip->getHitbox().intersects(getHitbox()))
	{
		Disappear();
		o_HeadsUpDisplay->AddCrystal();
	}

	mSprite.setPosition(position);
}

sf::Sprite Scrap::GetSprite()
{
	return mSprite;
}

float Scrap::GetX()
{
	return position.x;
}

float Scrap::GetY()
{
	return position.y;
}

bool Scrap::IsVisible()
{
	return mVisible;
}

void Scrap::Disappear()
{
	mVisible = false;
	active = false;
}

void Scrap::PickUp(bool ispicked)
{
	pickUp = ispicked;
}

bool Scrap::isPickedUp()
{
	return pickUp;
}

void Scrap::Activate(bool isActive)
{
	active = isActive;
}

bool Scrap::isActive()
{
	return active;
}

void Scrap::playPickUpSound()
{
	//Fix to right sound
}

sf::Vector2f Scrap::GetVPos()
{
	return position;
}

void Scrap::Draw(sf::RenderWindow & App)
{
	if(mVisible)
		App.draw(mSprite);
}

sf::FloatRect Scrap::getHitbox()
{
	return hitboxScrap;
}
