#include "stdafx.h"
#include "screen_options.h"
#include "cScreen.hpp"
#include "SoundManager.h"
#include <iostream>


screen_options::screen_options(void)
{
	assert(backgroundImage.loadFromFile("../data/options_menu.png"));
	assert(fullscreenTexture.loadFromFile("../data/fullscreen.png"));
	assert(audioTexture.loadFromFile("../data/audio.png"));
}

int screen_options::Run(sf::RenderWindow& App, sf::Clock deltaTime)
{
	view1.reset(sf::FloatRect(0, 0, 800, 600));
	bool Running = true;
	bool m_bfullscreen = false;
	int menu = 0;
	float volumeChange = 10;
	int Volume = 100;
	sf::Event Event;

	backgroundSprite.setTexture(backgroundImage);
	backgroundSprite.setTextureRect(sf::IntRect(0, 0, 800, 600));
	fullscreen.setTexture(fullscreenTexture);
	audio.setTexture(audioTexture);

	fullscreen.setPosition(view1.getCenter().x - fullscreen.getGlobalBounds().width / 2, view1.getCenter().y - fullscreen.getGlobalBounds().height * 3);
	audio.setPosition(view1.getCenter().x - audio.getGlobalBounds().width / 2, view1.getCenter().y - audio.getGlobalBounds().height);

	while (Running)
	{
		while (App.pollEvent(Event))
		{
			if (Event.type == sf::Event::Closed)
			{
				return (-1);
			}

			if (Event.type == sf::Event::KeyPressed)
			{
				switch (Event.key.code)
				{
				case sf::Keyboard::Escape:
					return 0;
					break;
				case sf::Keyboard::Down:
					menu = 1;
					break;
				case sf::Keyboard::Up:
					menu = 0;
					break;
				case sf::Keyboard::Right:
					if (menu == 1)
					{
						//Slide right
						Volume += volumeChange;
						if (Volume >= 100)
							Volume = 100;
						sf::Listener::setGlobalVolume(Volume);
					}
				case sf::Keyboard::Left:
					if (menu == 1)
					{
						//Slide left
						Volume -= volumeChange;
						if (Volume <= 0)
							Volume = 0;
						sf::Listener::setGlobalVolume(Volume);
					}
				case sf::Keyboard::Return:
					if (menu == 0)
					{
						m_bfullscreen = !m_bfullscreen;
						App.close();
						App.create(sf::VideoMode(1024, 768, 32), "Desolate Echo", m_bfullscreen ? Style::Fullscreen : Style::Resize | Style::Close);
						App.setFramerateLimit(60);
						App.setMouseCursorVisible(false);
					}
				default:
					break;
				}
			}
		}

		if (menu == 0)
		{
			fullscreen.setColor(sf::Color::White);
			audio.setColor(sf::Color(0 - 150 - 0));
		}
		else
		{
			fullscreen.setColor(sf::Color(0 - 150 - 0));
			audio.setColor(sf::Color::White);
		}
		App.clear();
		App.setView(view1);
		App.draw(backgroundSprite);
		App.draw(fullscreen);
		App.draw(audio);
		App.display();
	}
	return -1;
}
