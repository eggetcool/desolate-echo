#pragma once
#include "stdafx.h"

class Pause : public sf::Transformable
{
public:

	Pause();
	~Pause();

	bool Update(float deltatime);

	sf::Texture pauseScreenTexture;
	sf::Sprite pauseScreenSprite;

	sf::RenderTexture pauseScreenCanvasT;
	sf::Sprite pauseScreenCanvasS;

private:

	//sf::Sprite mSprite;
};