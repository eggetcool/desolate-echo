#pragma once

class aimAtMouse
{
public:
	aimAtMouse();
	float length(const Vector2f& rhs);
	void normalize(Vector2f& rhs);
	Vector2f normalized(const Vector2f& rhs);
	float to_angle(const Vector2f& rhs);

private:

};