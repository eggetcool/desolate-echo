#pragma once
#include "aimAtMouse.h"
class SpriteManager;
class AnimatedTexture;

class RocketLauncher
{
public:
	RocketLauncher(SpriteManager* pSpriteManager, Vector2f position, Vector2f direction, int viewsizeX, int viewsizeY);
	~RocketLauncher();
	void Update(float deltatime, Vector2i mousePos);
	sf::Sprite GetSprite();
	bool IsVisible();
	bool IsActive();
	void Deactivate();
	void playActivateSound();
	void Activate();
	void makeVisible(bool);
	void Move(float x, float y);
	Vector2f GetVPos();
	std::vector<RocketLauncher*> RocketLightV;
	sf::Sprite GetRocketLight();
	sf::FloatRect getHitbox();
	AnimatedTexture* GetTexture();
	void Animation();
	

private:
	RocketLauncher() {};

	sf::Event Event;
	sf::Vector2f position;
	sf::Vector2f direction;
	sf::Sprite mSprite;
	sf::Sprite RocketLight;
	sf::Texture RocketLightTexture;
	sf::FloatRect hitboxRocket;

	AnimatedTexture* explosionAnimation;
	AnimatedTexture* Rocketflying;
	aimAtMouse* mouseAim;

	float maxspeed;
	float currentSpeed;
	float accel;
	float timeElapsed;
	int width;
	int height;
	bool mVisible;
	bool mActive;

};
