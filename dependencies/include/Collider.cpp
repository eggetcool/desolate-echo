#include "stdafx.h"
#include "Collider.h"

Collider::Collider(int p_iWidth, int p_iHeight)
{
	m_xRegion.left = 0;
	m_xRegion.top = 0;
	m_xRegion.width = p_iWidth;
	m_xRegion.height = p_iHeight;
	//m_pxParent = nullptr;
}

void Collider::SetPosition(int p_iX, int p_iY)
{
	m_xRegion.left = p_iX;
	m_xRegion.top = p_iY;
}

void Collider::SetSize(int p_iWidth, int p_iHeight)
{
	if (p_iWidth < 0)
		p_iWidth = 0;
	if (p_iHeight < 0)
		p_iHeight = 0;
	m_xRegion.width = p_iWidth;
	m_xRegion.height = p_iHeight;
}

int Collider::GetX()
{
	return m_xRegion.left;
}

int Collider::GetY()
{
	return m_xRegion.top;
}

int Collider::GetW()
{
	return m_xRegion.width;
}

int Collider::GetH()
{
	return m_xRegion.height;
}

void Collider::Refresh()
{

}
