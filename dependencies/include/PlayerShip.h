#pragma once
#include "SoundManager.h"
class aimAtMouse;
class SoundManager;
class Prowler;
class MapLoad;
class SpriteManager;
class AnimatedTexture;
class Shield;
//class Collider;

class PlayerShip : public sf::Transformable
{
public:
	PlayerShip(SpriteManager* pSpriteManager, int pScreenWidth, SoundManager* o_Soundmanager);
	~PlayerShip();
	void Update(float deltatime);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	int Life(int);
	bool IsVisible();
	void NearDeath();
	void Death();
	void Move(float x, float y);
	void Hit(sf::Vector2f enemyPos);
	sf::Vector2f GetVPos();
	void SetVPos(sf::Vector2f setPosition);
	float GetDir();
	void Draw(sf::RenderWindow& App);
	sf::FloatRect getHitbox();
	//Collider* GetCollider();
	float GetLife();
	void shieldPickedUp(bool value);
	bool isShieldPicked();
	void Animation();
	void stopSounds();

	sf::Vector2f velocity;
private:
	PlayerShip() {};

	aimAtMouse*			mouseAim;
	Prowler*			o_Prowler;
	SoundManager*		o_Soundmanager;
	MapLoad*			tile;
	Shield*				o_Shield;
	AnimatedTexture*	forward;
	AnimatedTexture*	backwards;
	AnimatedTexture*	right;
	AnimatedTexture*	left;
	AnimatedTexture*	idle;
	AnimatedTexture*	ShieldRecharge;
	AnimatedTexture*	ShieldHit;
	AnimatedTexture*	ShieldDepleted;
	/*Collider*			mCollider;*/

	sf::FloatRect hitboxShip;
	sf::Sprite mSprite;
	sf::Sprite mSpriteSheetShield;
	sf::Event Event;
	sf::Vector2f position;
	sf::Vector2f direction;
	sf::Sound* sound;
	sf::Sound* hoverThrusterSound;
	sf::Sound* hoverThrusterSoundBack;
	sf::Sound* hoverThrusterSoundLeft;
	sf::Sound* hoverThrusterSoundRight;
	sf::Sound* hoverDeathSound;


	float mX;
	float mY;
	float maxspeed = 3.0f;
	float accel = 0.05f;
	float decel = 0.975f;
	float accelB = 0.01f;
	float life;
	float angle;
	float animationTimer;
	float animationTimerhit;
	int mScreenWidth;
	bool mVisible;
	bool changeAnimation;
	bool mDead;
	bool pressed;
	bool pressedS;
	bool pressedRight;
	bool pressedLeft;
	bool lastPressed;
	bool lastPressedS;
	bool lastPressedRight;
	bool lastPressedLeft;
	bool shieldPicked = false;
	bool shieldActive = false;
	bool shieldRecharge = false;
	bool hit = false;
};