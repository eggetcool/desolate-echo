#include "stdafx.h"
#include "MapLoad.h"
#include "TileMap.h"
#include <fstream>

MapLoad::MapLoad()
{
	
}

void MapLoad::loadTiles()
{
	std::fstream infile;
	std::string line;
	infile.open("../data/Level 2.csv");
	/*char charlevel[72900];
	infile >> charlevel;*/

	int index = 0;
	int x = 0;
	int y = 0;
	while (std::getline(infile, line))
	{
		int commaIndex = line.find(',');
		int size = line.size();
		for (int i = 0; i < size; i++)
		{
			commaIndex = line.find(',');
			std::string sub = line.substr(0, commaIndex);
			line.erase(0, commaIndex + 1);


			char* c = &sub[0];
			int nr = atoi(c);

			level2D[y][x] = nr;
			if (x > 100)
			{
				int rawr = 0;
			}
			x++;
		}
		x = 0;
		y++;
	}
	/*for (int x = 0; x < sizeof(charlevel); x++)
	{
		int nr = (charlevel[x] - '0') % 48;
		if (nr < 0)
		{
			nr = (charlevel[x + 1] - '0') % 48;
		}
		level[x] = nr;
	}*/
	infile.close();
	//return level;
}

void MapLoad::Draw(sf::RenderWindow & App, int xStart, int xEnd, int yStart, int yEnd)
{
	//App.draw(map);

	sf::Texture tex;
	tex.loadFromFile("../data/tileset_1.png");
	sf::Sprite sprite;
	sprite.setTexture(tex);
	xEnd += 1;
	yEnd += 1;
	if (xStart < 0)
		xStart = 0;
	if (xEnd >= 270)
		xEnd = 270;
	if (yStart < 0)
		yStart = 0;
	if (yEnd >= 220)
		yEnd = 220;

	for (int x = xStart; x < xEnd; x++)
	{
		for (int y = yStart; y < yEnd; y++)
		{
			int id = level2D[y][x];

			int xTile = id % 16;
			int yTile = id / 16;

			sprite.setTextureRect(sf::IntRect(32 * xTile, 32 * yTile, 32, 32));
			sprite.setPosition(x * 32, y * 32);
			App.draw(sprite);
		}
	}
}

//std::vector<const int> MapLoad::GetTilesV()
//{
//	/*for (int i = 0; i < (sizeof(level) / sizeof(*level)))
//	{
//		tryck in skiten i tiles;
//		tack c++;
//	}*/
//}