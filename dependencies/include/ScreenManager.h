#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>

class ScreenManager
{
public:
	virtual int Run(sf::RenderWindow &App) = 0;
};