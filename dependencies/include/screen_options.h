#pragma once
#include <iostream>
#include "stdafx.h"
#include "cScreen.hpp"
#include "SoundManager.h"

class screen_options : public cScreen
{
public:
	screen_options(void);
	virtual int Run(sf::RenderWindow &App, sf::Clock deltaTime);

private:
	sf::View view1;
	sf::Texture backgroundImage;
	sf::Texture fullscreenTexture;
	sf::Texture audioTexture;
	sf::Sprite backgroundSprite;
	sf::Sprite fullscreen;
	sf::Sprite audio;
};