#pragma once


class AnimatedTexture;

class SpriteManager
{
public:
	SpriteManager();
	~SpriteManager();


	AnimatedTexture* CreateAnimatedTexture(const std::string& p_sFilepath);

private:
	std::map<std::string, sf::Texture> m_apxTextures;
	std::vector<AnimatedTexture*> m_apxAnimatedTextures;
};