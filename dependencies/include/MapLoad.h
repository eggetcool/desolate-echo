#pragma once

#include "TileMap.h"
//class TileMap;

class MapLoad
{
public:
	MapLoad();
	void loadTiles();
	void Draw(sf::RenderWindow & App, int xStart, int xEnd, int yStart, int yEnd);
	int level2D[220][270];
private:
	TileMap map;
	//std::vector<const int> tiles;
	int level[59400];
};