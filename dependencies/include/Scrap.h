#pragma once
#include "SoundManager.h"

class PlayerShip;
class HeadsUp;
class SoundManager;

class Scrap
{
public:
	Scrap(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f crystalPos, PlayerShip* o_PlayerShip, HeadsUp* o_HeadsUpDisplay, SoundManager* o_Soundmanager);
	~Scrap();
	void Update(float deltatime);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	bool IsVisible();
	void Disappear();
	void PickUp(bool);
	bool isPickedUp();
	void Activate(bool);
	bool isActive();
	void playPickUpSound();
	sf::Vector2f GetVPos();
	void Draw(sf::RenderWindow& App);
	sf::FloatRect getHitbox();
private:
	bool pickUp;
	bool active;
	bool mVisible;
	int mScreenWidth;

	sf::Sprite mSprite;
	sf::Vector2f shipPosition;
	sf::Vector2f position;
	sf::FloatRect hitboxScrap;
	PlayerShip* o_PlayerShip;
	HeadsUp* o_HeadsUpDisplay;
	SoundManager* soundmanager;

	sf::Sound* scrapChargedSound;
};