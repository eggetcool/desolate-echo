#pragma once

class Collider;

class CollisionManager
{
public:
	static bool Check(sf::FloatRect p_pxLeft, sf::FloatRect p_pxRight,
		int& p_iOverlapX, int& p_iOverlapY);
};