#include "stdafx.h"
#include "Sprite.h"
#include "aimAtMouse.h"
#include "RocketLauncher.h"
#include "SpriteManager.h"
#include "AnimatedTexture.h"
#include "MathExtension.h"

RocketLauncher::RocketLauncher(SpriteManager* pSpriteManager, Vector2f position, Vector2f direction, int windowsizeX, int windowsizeY)
{
	Rocketflying = pSpriteManager->CreateAnimatedTexture("../data/missile_fired.png");
	explosionAnimation = pSpriteManager->CreateAnimatedTexture("../data/spritesheet_explosion.png");
	Animation();
	mSprite.setTextureRect(Rocketflying->GetRegion());
	mSprite.setTexture(Rocketflying->GetTexture());

	mSprite.setTextureRect(explosionAnimation->GetRegion());
	mSprite.setTexture(explosionAnimation->GetTexture());

	mVisible = true;
	mActive = true;
	maxspeed = 30.0f;
	currentSpeed = 0.5f;
	timeElapsed = 0;
	accel = 1.5f;
	width = windowsizeX;
	height = windowsizeY;
	this->direction = direction;
	this->position = position;

	mSprite.setPosition(position);
	mSprite.setOrigin(((mSprite.getLocalBounds().width / 2) - 15), (mSprite.getLocalBounds().height / 2));
	mSprite.setScale(1.5, 1.5);

	RocketLightTexture.loadFromFile("../data/pointLightTexture.png");
	RocketLightTexture.setSmooth(true);

	RocketLight.setTexture(RocketLightTexture);
	RocketLight.setPosition(GetVPos().x, GetVPos().y);
	RocketLight.setOrigin((RocketLight.getGlobalBounds().width / 4), (RocketLight.getGlobalBounds().height / 4));
	RocketLight.setScale(1, 1);
	//RocketLight.setOrigin(36.f, 8.f);
	RocketLight.setColor(sf::Color::Yellow);
	
}

RocketLauncher::~RocketLauncher()
{
}

void RocketLauncher::Update(float deltatime, Vector2i mousePos)
{
	hitboxRocket = mSprite.getGlobalBounds();
	hitboxRocket.top = position.y;
	hitboxRocket.left = position.x;

	//sf::RectangleShape hitboxOutline(sf::Vector2f(hitboxProjectile.left, hitboxProjectile.top));
	//hitboxOutline.setFillColor(sf::Color(100, 250, 50));
	//hitboxOutline.setPosition(position.x, position.y);

	position += direction * maxspeed * deltatime;
	mSprite.setPosition(position);
	Rocketflying->Update(deltatime);
	mSprite.setTextureRect(Rocketflying->GetRegion());
	mSprite.setTexture(Rocketflying->GetTexture());
	float angle = MathEx::to_angle(direction);
	mSprite.setRotation(angle + 90);
	RocketLight.setPosition(position.x - 16.f, position.y - 16.f);
	if (!mVisible)
	{
		timeElapsed += deltatime;
		if (timeElapsed <= 0.9f)
		{
			explosionAnimation->Update(deltatime);
			mSprite.setTextureRect(explosionAnimation->GetRegion());
			mSprite.setTexture(explosionAnimation->GetTexture());
		}
		else
		{
			mActive = false;
		}
	}
	if (position.x > 8640)
	{
		mVisible = false;
	}
	if (position.y > 7040)
	{
		mVisible = false;
	}
	if (position.x < 0)
	{
		mVisible = false;
	}
	if (position.y < 0)
	{
		mVisible = false;
	}
}

sf::Sprite RocketLauncher::GetSprite()
{
	return mSprite;
}

bool RocketLauncher::IsVisible()
{
	return mVisible;
}

bool RocketLauncher::IsActive()
{
	return mActive;
}

void RocketLauncher::Deactivate()
{
	mActive = false;
}

void RocketLauncher::playActivateSound()
{
	//Fix to the right sound
}

void RocketLauncher::Activate()
{
	mActive = true;
}

void RocketLauncher::makeVisible(bool value)
{
	mVisible = value;
}

void RocketLauncher::Move(float x, float y)
{
	position.x = position.x + x;
	position.y = position.y + y;
}

Vector2f RocketLauncher::GetVPos()
{
	return position;
}

sf::Sprite RocketLauncher::GetRocketLight()
{
	return RocketLight;
}

sf::FloatRect RocketLauncher::getHitbox()
{
	return hitboxRocket;
}

AnimatedTexture* RocketLauncher::GetTexture()
{
	return Rocketflying;
}

void RocketLauncher::Animation()
{
	float framefloat = 0.09;
	Rocketflying->AddFrame(0, 0, 32, 32, framefloat);
	Rocketflying->AddFrame(33, 0, 32, 32, framefloat);
	Rocketflying->AddFrame(66, 0, 32, 32, framefloat);
	Rocketflying->AddFrame(0, 33, 32, 32, framefloat);

	explosionAnimation->AddFrame(0, 0, 64, 64, framefloat);
	explosionAnimation->AddFrame(65, 0, 64, 64, framefloat);
	explosionAnimation->AddFrame(0, 65, 64, 64, framefloat);
	explosionAnimation->AddFrame(65, 65, 64, 64, framefloat);
	explosionAnimation->AddFrame(0, 130, 64, 64, framefloat);
	explosionAnimation->AddFrame(65, 130, 64, 64, framefloat);
	explosionAnimation->AddFrame(130, 65, 64, 64, framefloat);
	explosionAnimation->AddFrame(130, 130, 64, 64, framefloat);
	explosionAnimation->AddFrame(130, 0, 64, 64, framefloat);
}
